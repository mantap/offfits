<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/db_connect.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/functions.php';

  sec_session_start();

  head_tag();
  page_header($mysqli);

?>

<!-- Main Page Cover Carousel -->
  <div id="cover-carousel" class="carousel slide carousel-cover" data-rise="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#cover-carousel" data-slide-to="0" class="active"></li>
      <li data-target="#cover-carousel" data-slide-to="1"></li>
      <li data-target="#cover-carousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <!-- search-form -->
      <form class="form-inline search-form" role="form" action="list.php" method="get">
        <h1>Ayo, cari tempat kerja favorit mu!</h1>

        <br>

        <div id="jobcompanysearchbar" class="form-group">
          <input id="jobcompany" list="autocomplete" type="text" class="form-control" name="company_name" placeholder="i.e. Job title or company's name">
        </div>
        <div class="form-group">
          <input type="text" class="form-control" name="company_location" placeholder="Location">
        </div>
        <button type="submit" class="btn btn-primary">Search</button>

      </form> <!-- /search-form-->


      <div class="item active">
        <img class="cover-pic" src="img/carousel-pic1.jpg" alt="...">
        <div class="carousel-caption">
          <h3>Jakarta</h3>
          <p>This is Main Picture 1</p>
        </div>
      </div>
      <div class="item">
        <img class="cover-pic" src="img/eeb.jpg" alt="...">
        <div class="carousel-caption">
          <h3>UW EE Building</h3>
          <p>This is Main Picture 2</p>
        </div>
      </div>
      <div class="item">
        <img class="cover-pic" src="img/eeb.jpg" alt="...">
        <div class="carousel-caption">
          <h3>UW EE Building</h3>
          <p>This is Main Picture 3</p>
        </div>
      </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#cover-carousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#cover-carousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
  </div> <!-- /Main Page Cover Carousel -->



<?php
page_footer();
?>