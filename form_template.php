<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/db_connect.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/functions.php';

  sec_session_start();

  head_tag();
  page_header($mysqli);

?>



  <!-- Main Container -->
  <div class="container prof-main-container">
    <!-- left main container -->
    <div class="left-main-container">

  <?php if (login_check($mysqli) == true) : ?>
      <!-- Review From Tabs -->
      <p class="section-heading">Form Review PT. Jaya Makmur Sukma Bakti</p>
      <p>* Kami sadar kalau beberapa informasi di bawah sangat sensitif sekali. Untuk menghormati privasi anda, nama atau username anda tidak akan ditampilkan di review page nanti.</p> 




<br>

<!-- return button -->
<form method="return-btn-link" action="profile.php">
  <button type="return-btn" class="btn btn-default">Go Back</button>
</form>

<br>

<?php else : ?>
    <p>
        <span class="error">You are not authorized to access this page.</span> Please <a href="login.php">login</a>.
    </p>
<?php endif; ?>


    </div> <!-- /left main container -->

    <!-- right main container -->
    <div class="right-main-container">

      <br>

    </div> <!-- /right main container -->
  </div> <!-- / Main container -->

  <br>
  <br>

<?php
page_footer();
?>