<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/db_connect.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/functions.php';

  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/lib/Classes/Paginator/paginator.php';

  sec_session_start();

  head_tag();
  page_header($mysqli);


  $search_company_name = ( isset( $_GET['company_name'] ) ) ? $_GET['company_name'] : "";
  $search_company_location = ( isset( $_GET['company_location'] ) ) ? $_GET['company_location'] : "";

  $extra_param = "";
  $extra_param .= ( isset( $_GET['company_name'] ) ) ? ('&company_name=' . $search_company_name) : "";
  $extra_param .= ( isset( $_GET['company_location'] ) ) ? ('&company_location=' . $search_company_location) : "";

  $conn       = new mysqli( 'localhost', 'root', 'root', 'offfits_data_db' );

  $limit      = ( isset( $_GET['limit'] ) ) ? $_GET['limit'] : 25;
  $page       = ( isset( $_GET['page'] ) ) ? $_GET['page'] : 1;
  $links      = ( isset( $_GET['links'] ) ) ? $_GET['links'] : 7;
  $query      = "SELECT company_id, company_name, company_address_1, company_city " .
              "FROM company_profile_table " .
              "WHERE (company_address_1 LIKE '%" . $search_company_location . "%' OR " .
                  "company_city LIKE '%" . $search_company_location . "%' OR " .
                  "company_address_2 LIKE '%" . $search_company_location . "%' ) " .
              "AND (company_name LIKE '%" . $search_company_name . "%') ";

  $Paginator  = new paginator( $conn, $query );

  $results    = $Paginator->getData( $limit, $page );


?>



  <!-- Main Container -->
  <div class="container prof-main-container">
    <!-- left main container -->
    <div class="left-main-container">

      <br>

                      <div class="col-md-10 col-md-offset-1">
                <h1>Search Result</h1>
                <table class="table table-striped table-condensed table-bordered table-rounded">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th width="30%">Name</th>
                            <th width="60%">Address</th>
                            <th width="5%">City</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for( $i = 0; $i < count( $results->data ); $i++ ): ?>
                            <tr>
                                <td><?= $results->data[$i]['company_id'] ?></td>
                                <td><a href="profile.php?company_id=<?= $results->data[$i]['company_id'] ?>"><?= $results->data[$i]['company_name'] ?></a></td>
                                <td><?= $results->data[$i]['company_address_1'] ?></td>
                                <td><?= $results->data[$i]['company_city'] ?></td>
                            </tr>
                        <?php endfor; ?>
                    </tbody>
                </table>
                    <?php echo $Paginator->createLinks( $links, 'pagination pagination-sm', $extra_param ); ?>
                </div>


    </div> <!-- /left main container -->

    <!-- right main container -->
    <div class="right-main-container">

      <br>

    </div> <!-- /right main container -->
  </div> <!-- / Main container -->

  <br>
  <br>

<?php
page_footer();
?>