<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/db_connect.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/functions.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/forms.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits//lib/Classes/DB_Manager/db_manager.php';

  sec_session_start();

  head_tag();
  page_header($mysqli);

  $company_id = $_POST['company_id'];
  $company_name = $_POST['company_name'];

?>



  <!-- Main Container -->
  <div class="container prof-main-container">
    <!-- left main container -->
    <div class="left-main-container">

  <?php if (login_check($mysqli) == true) : ?>
      <!-- Review From Tabs -->
      <p class="section-heading">Form Review <?= $company_name ?></p>
      <p>* Kami sadar kalau beberapa informasi di bawah sangat sensitif sekali. Untuk menghormati privasi anda, nama atau username anda tidak akan ditampilkan di review page nanti.</p> 

      <!-- Nav tabs -->
      <div class="row">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#form_review" data-toggle="tab">Review Perusahaan</a></li>
          <li><a href="#form_interview" data-toggle="tab">Review Proses Interview</a></li>
          <li><a href="#form_salary" data-toggle="tab">Review Gaji</a></li>
          <li><a href="#form_upload_photos" data-toggle="tab">Upload Photos</a></li>
        </ul>
      </div>

      <!-- Tab panes -->
      <div class="tab-content">
        <!-- Tab Review Perusahaan -->
        <div class="tab-pane active" id="form_review">
          <?php printReviewForm( $company_id ); ?>
        </div> <!-- /Tab Review Perusahaan -->
        <!-- Tab Review Process Interview -->
        <div class="tab-pane" id="form_interview">
          <?php printInterviewForm( $company_id ); ?>
        </div> <!-- /Tab Review Process Interview -->
        <!-- Tab Review Gaji -->
        <div class="tab-pane" id="form_salary">
          <?php printInfoGajiForm( $company_id); ?>
        </div> <!-- /Tab Review Gaji -->
        <!-- Tab Upload Photos -->
        <div class="tab-pane" id="form_upload_photos">
          <?php printUploadFotoForm( $company_id ); ?>
        </div>

      </div> <!-- /Tab panes -->

<br>

<!-- return button -->
<form method="return-btn-link" action="profile.php">
  <input style="display:none;" name="company_id" value="<?php echo $company_id; ?>">
  <button type="return-btn" class="btn btn-default">Go Back</button>
</form>

<br>

<?php else : ?>
    <p>
        <span class="error">You are not authorized to access this page.</span> Please <a href="login.php">login</a>.
    </p>
<?php endif; ?>


    </div> <!-- /left main container -->

    <!-- right main container -->
    <div class="right-main-container">

      <br>

    </div> <!-- /right main container -->
  </div> <!-- / Main container -->

  <br>
  <br>

<?php
page_footer();
?>