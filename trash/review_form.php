<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/db_connect.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/functions.php';

  sec_session_start();

  head_tag();
  page_header($mysqli);

?>



  <!-- Main Container -->
  <div class="container prof-main-container">
    <!-- left main container -->
    <div class="left-main-container">

  <?php if (login_check($mysqli) == true) : ?>
      <!-- Review From Tabs -->
      <p class="section-heading">Form Review PT. Jaya Makmur Sukma Bakti</p>
      <p>* Kami sadar kalau beberapa informasi di bawah sangat sensitif sekali. Untuk menghormati privasi anda, nama atau username anda tidak akan ditampilkan di review page nanti.</p> 


          <form role="form">

            <div class="form-group">
              <label for="overall-rev-perusahaan">Overall Rating Perusahaan</label>
              <select class="form-control">
                <option>5</option>
                <option>4</option>
                <option>3</option>
                <option>2</option>
                <option>1</option>
              </select>
              <span class="help-block">5 = Baik Sekali, 1 = Buruk Sekali</span>
            </div>

            <div class="form-group">
              <label for="reviewTitle">Review Title</label>
              <input type="review-title" class="form-control" id="reviewTitle" placeholder="Silahkan masukan judul review anda">
            </div>

            <div class="form-group">
              <label for="job-title">Posisi Pekerjaan</label>
              <input type="job-title" class="form-control" id="job-title" placeholder="i.e. Karyawan Lvl 1">
            </div>

            <div class="form-group">
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                  Masih bekerja di perusahaan ini
                </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                  Sudah tidak bekerja di perusahaan ini
                </label>
              </div>
            </div>

            <div class="form-group">
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios2" id="optionsRadios3" value="option1" checked>
                  Pegawai Tetap
                </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios2" id="optionsRadios4" value="option2">
                  Pegawai Kontrak
                </label>
              </div>
            </div>

            <div class="form-group">
             <label for="pros">Kelebihan</label>
             <textarea class="form-control" rows="2"></textarea>
           </div>

           <div class="form-group">
             <label for="cons">Kekurangan</label>
             <textarea class="form-control" rows="2"></textarea>
           </div>

           <div class="form-group">
             <label for="advice">Masukan untuk Manajemen</label>
             <textarea class="form-control" rows="2"></textarea>
           </div>

           <div class="form-group">
             <label for="reviewTitle">Peluang Kemajuan Karir</label>
             <select class="form-control">
              <option>5</option>
              <option>4</option>
              <option>3</option>
              <option>2</option>
              <option>1</option>
            </select>
            <span class="help-block">5 = Besar Sekali, 1 = Tidak ada peluang sama sekali</span>
          </div>

          <div class="form-group">
           <label for="reviewTitle">Kompensasi dan Bonus</label>
           <select class="form-control">
            <option>5</option>
            <option>4</option>
            <option>3</option>
            <option>2</option>
            <option>1</option>
          </select>
          <span class="help-block">5 = Baik Sekali, 1 = Buruk Sekali</span>
        </div>

        <div class="form-group">
          <label for="reviewTitle">Kesibukan Kerja</label>
          <select class="form-control">
            <option>5</option>
            <option>4</option>
            <option>3</option>
            <option>2</option>
            <option>1</option>
          </select>
          <span class="help-block">5 = Selalu Lembur, 1 = Tidak pernah lembur</span>
        </div>

        <div class="form-group">
          <label for="reviewTitle">Manajemen</label>
          <select class="form-control">
            <option>5</option>
            <option>4</option>
            <option>3</option>
            <option>2</option>
            <option>1</option>
          </select>
          <span class="help-block">5 = Baik Sekali, 1 = Buruk Sekali</span>
        </div>

        <div class="form-group">
          <label for="reviewTitle">Suasana Kerja</label>
          <select class="form-control">
            <option>5</option>
            <option>4</option>
            <option>3</option>
            <option>2</option>
            <option>1</option>
          </select>
          <span class="help-block">5 = Nyaman Sekali, 1 = Tidak Nyaman Sama Sekali</span>
        </div>

        <button type="submit" class="btn btn-default">Submit</button>

      </form> <!-- /form review perusahaan -->

<br>

<!-- return button -->
<form method="return-btn-link" action="profile.php">
  <button type="return-btn" class="btn btn-default">Go Back</button>
</form>

<br>

<?php else : ?>
    <p>
        <span class="error">You are not authorized to access this page.</span> Please <a href="login.php">login</a>.
    </p>
<?php endif; ?>


    </div> <!-- /left main container -->

    <!-- right main container -->
    <div class="right-main-container">

      <br>

    </div> <!-- /right main container -->
  </div> <!-- / Main container -->

  <br>
  <br>

<?php
page_footer();
?>