<?php
include_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/db_connect.php';
include_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/functions.php';

	function head_tag() {
		?>

		<!DOCTYPE html>
  			<html lang="en">
  				<head>
    				<meta charset="utf-8">
    				<meta http-equiv="X-UA-Compatible" content="IE=edge">
    				<meta name="viewport" content="width=device-width, initial-scale=1">
    				<meta name="description" content="">
   					<meta name="author" content="">
   					<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    					
    				<title>OffFits - Finding The Best Job</title>

    				<!-- Bootstrap core CSS -->
				    <link href="css/bootstrap.min.css" rel="stylesheet">

				    <!-- Custom styles for this template -->
				    <link href="css/gaya.css" rel="stylesheet">

				    <!-- Just for debugging purposes. Don't actually copy this line! -->
					<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

					<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
					<!--[if lt IE 9]>
					<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
					<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
					<![endif]-->

					<script src="search_bar.js" type="text/javascript"></script>

				</head>
		<?php
	}

	function page_header() {
		?>

		<body>

    		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      			<div class="container">
        			<div class="navbar-header">
         				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            				<span class="sr-only">Toggle navigation</span>
            				<span class="icon-bar"></span>
           					<span class="icon-bar"></span>
            				<span class="icon-bar"></span>
          				</button>
          				<a class="navbar-brand" href="index_temp.php">OffFits</a>
        			</div>

        			<div class="navbar-collapse collapse">
          				<ul class="nav navbar-nav">
            				<li class="active"><a href="index_temp.php">Home</a></li>
            				<li><a href="profile_temp.php">Profil Usaha</a></li>

          					<!--
          						<li><a href="#about">About</a></li>
          						<li><a href="#contact">Contact</a></li>
          						<li class="dropdown">
            						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
            						<ul class="dropdown-menu">
              							<li><a href="#">Action</a></li>
              							<li><a href="#">Another action</a></li>
              							<li><a href="#">Something else here</a></li>
              							<li class="divider"></li>
              							<li class="dropdown-header">Nav header</li>
              							<li><a href="#">Separated link</a></li>
              							<li><a href="#">One more separated link</a></li>
            						</ul>
          						</li>
          					-->
          
        				</ul>

        				<?php 
        					if(login_check($mysqli) == true) {
        						welcome_back_user();
        					} else {
        						sign_in_form();
        					}
        				?>

      				</div><!--/.navbar-collapse -->
    			</div>
  			</div>

		<?php
	}

	function page_footer() {
		?>

				<footer>
		  			<!-- footer container-->
		  			<div class="container footer-container">
		    			<!-- 1st row -->
		    			<div class="row">
		      				<!-- 1st column -->
		      				<div class="col-xs-2">
		        				<ul class="footer-contents">
		          					<li><a href="#">About</a></li>
		          					<li><a href="#">Contact</a></li>
		          					<li><a href="#">Press</a></li>
		        				</ul>
		      				</div> <!-- /1st column -->
		      				<!-- 2nd column -->
		      				<div class="col-xs-2">
		        				<ul class="footer-contents">
		         					<li><a href="#">Help</a></li>
		          					<li><a href="#">FAQ</a></li>
		          					<li><a href="#">Terms & Policy</a></li>
		        				</ul>
		      				</div> <!-- /2nd column -->
		   				 </div>  <!-- /1st row -->
		    			<!-- 2nd row -->
		   				 <div class="row">
		      				<p class="trademark">&copy; OffFits 2014</p>
		    			</div>  <!-- /2nd row -->
		  			</div> <!-- /footer container -->
				</footer>

		  		<!-- Bootstrap core JavaScript
		  		================================================== -->
		  		<!-- Placed at the end of the document so the pages load faster -->
  				<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  				<script src="js/bootstrap.min.js"></script>
			</body>
		</html>

		<?php
	}

	function activity_news_tab() {
		?>

			<!-- right main container -->
	    	<div class="right-main-container">
	      		<!-- 1st row -->
	      		<p class="section-heading">Aktivitas Terbaru</p>
	      			<div class="row">
	        			<div class="right-main-container-sm">
	          				<p class="section-title">
	            			Heading
	          				</p>
	          				<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
	          				<p class="section-title">
	            			Heading
	          				</p>
	          				<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
	        			</div> <!-- /right-main-container-sm -->
	      			</div> <!-- /1st row -->

	      		<!-- 2nd row -->
	      		<p class="section-heading">Inspirasi</p>
	      			<div class="row">
	        			<div class="right-main-container-sm">
	          				<h4>Heading</h4>
	          				<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
	         				 <h4>Heading</h4>
	          				<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
	        			</div> <!-- /right-main-container-sm -->
	      			</div> <!-- /2nd row -->

	    	</div> <!-- /right main container -->
  		</div> <!-- /prof-main-container -->

  		<br>
 		<br>

		<?php
	}

	function sign_in_form() {
		?>

		<form class="navbar-form navbar-right" role="form" action="sign_in.php" method="post">
          	<div class="form-group">
           		<input type="text" placeholder="Email" class="form-control" name="username">
         	</div>
          	<div class="form-group">
            	<input type="password" placeholder="Password" class="form-control" name="password">
         	</div>
          	<button type="submit" class="btn btn-success">Sign in</button>
        </form>

		<?php
	}

	function welcome_back_user(){
		echo '<p>Currently logged ' . $logged . ' as ' . htmlentities($_SESSION['username']) . '.</p>';
	}


?>