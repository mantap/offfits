<?php

  function show_data($row, $name) {
    ?>

      <span class="prof-info-label"><b><?=$name?></b></span>

    <?php
    if($name == "Website") {
      ?>

        <span class="prof-info-data"><a href="<?=$row[$name]?>">Here</span>

      <?php
    } else {
      ?>

        <span class="prof-info-data"><?=$row[$name]?></span>

      <?php
    } 
  }

  $company = $_POST["company"];
  $db = new PDO("mysql:dbname=Offfits;host=offfits.com", "mcindy", "021pipoputih");
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $company = $db->quote("%".$company."%");
  $rows = $db->query("SELECT * FROM DaftarPerusahaan 
                      WHERE Nama LIKE $company 
                      LIMIT 1");
  $row = $rows->fetch();

  include("common.php");
  head_tag();
  page_header();
?>


<div class="container prof-main-container">
  <!-- left main container -->
  <div class="left-main-container">
    <!-- main profile page row -->
    <div class="row">
      <div class="col-md-8">
        <p class="section-heading"><?=$row["Nama"]?></p>
        <!-- Keterangan Perusahaan -->
        <label class="prof-rating"><b>Overall Rating </b></label>
        <span class="small-text">(20 reviews)</span> 
        <!-- rating bar -->
        <div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 92%;"> 92%
          </div>
        </div><!-- /rating bar -->

        <br>

        <?php
          show_data($row, "Lokasi");
          show_data($row, "Jumlah Karyawan");
          show_data($row, "Tahun");
          show_data($row, "Tipe");
          show_data($row, "Industri");
          show_data($row, "Presiden Direktur");
          show_data($row, "Website");
        ?>

        <!--
        <span class="prof-info-label"><b>Lokasi</b></span>
        <span class="prof-info-data">18th floor Tower II</span>
        <span class="prof-info-label"></span>
        <span class="prof-info-data">Jakarta Stock Exchange Building</span>
        <span class="prof-info-label"></span>
        <span class="prof-info-data">Sudirman Central Business </span>
        <span class="prof-info-label"></span>
        <span class="prof-info-data">Jl.Jend.Sudirman Kav.52-53</span>
        <span class="prof-info-label"></span>
        <span class="prof-info-data">Jakarta 12190</span>
        <span class="prof-info-label"></span>
        <span class="prof-info-data">Indonesia</span>

        <span class="prof-info-label"><b>Jumlah Karyawan</b></span>
        <span class="prof-info-data">80 - 100 orang</span>
        <label class="prof-info-label"><b>Tahun Dibangun</b></label>
        <span class="prof-info-data">1996</span>
        <label class="prof-info-label"><b>Tipe</b></label> 
        <span class="prof-info-data">Public</span>
        <label class="prof-info-label"><b>Industri</b></label>
        <span class="prof-info-data">Teknologi Informasi</span>
        <label class="prof-info-label"><b>President Direktur</b></label>
        <span class="prof-info-data">Andreas Diantoro</span>
        <label class="prof-info-label"><b>Website</b></label>
        <span class="prof-info-data"><a href="http://www.microsoft.com/id-id/default.aspx">http://www.microsoft.com</span>
        -->

      </div><!-- /Keterangan Perusahaan -->
      <div class="col-md-4">
        <img class="img-thumbnail prof-pic" src="img/eeb.jpg">
        <a herf="#">more photos...</a>
      </div>
    </div>

    <!-- row for map -->
    <div class="row map-container">
      <iframe width="100%" height="200" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAwrCdwLrFyM84znFGmQldsZmSX7gfQl1Y&q=microsoft+indonesia+jakarta">
      </iframe>
    </div> 

    <br>

    <!-- Nav tabs -->
    <div class="row">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#reviews" data-toggle="tab">Review Perusahaan</a></li>
        <li><a href="#interviews" data-toggle="tab">Interviews</a></li>
        <li><a href="#salaries" data-toggle="tab">Gaji</a></li>
        <li><a href="#jobs" data-toggle="tab">Lowongan Kerja</a></li>
      </ul>
    </div>

    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Tab Review Perusahaan -->
      <div class="tab-pane active" id="reviews">
        <!-- profile page user review container -->
        <div class="row prof-usr-review-container">
          <div class="col-md-5">
            <div class="col-xs-6">
              <img class="img-thumbnail user-prof-pic-thumb" src="img/eeb2.png">
            </div>
            <div class="col-xs-6">
              <h5 class="text-blue">Tukul Arwani</h5>
              <h6>Bogor,</h6>
              <h6>Bogor</h6>
              </div>
            </div>
            <div class="col-md-7">
              <div class="usr-review-rating">
                <h6>Rating : 
                  <span class="above-85">92%</span>
                </h6>
              </div>
              <div class="usr-review-date">
                <h6>4/6/2014</h6>
              </div> 

              <br>
              <br>

              <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            </div>
          </div> <!-- /row /profile page user review container -->
          <!-- profile page user review container -->
          <div class="row prof-usr-review-container">
            <div class="col-md-5">
              <div class="col-xs-6">
                <img class="img-thumbnail user-prof-pic-thumb" src="img/eeb2.png">
              </div>
              <div class="col-xs-6">
                <h5 class="text-blue">Andi Kurus</h5>
                <h6>Jakarta Barat,</h6>
                <h6>DKI Jakarta</h6>
              </div>
            </div>
            <div class="col-md-7">
              <div class="usr-review-rating">
                <h6>Rating : 
                  <span class="between-55-85">84%</span>
                </h6>
              </div>
              <div class="usr-review-date">
                <h6>4/4/2014</h6>
              </div> 

              <br>
              <br>

              <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            </div>
          </div> <!-- /row /profile page user review container -->
          <!-- profile page user review container -->
          <div class="row prof-usr-review-container">
            <div class="col-md-5">
              <div class="col-xs-6">
                <img class="img-thumbnail user-prof-pic-thumb" src="img/eeb2.png">
              </div>
              <div class="col-xs-6">
                <h5 class="text-blue">Budi Tulus</h5>
                <h6>Surabaya Barat,</h6>
                <h6>Surabaya</h6>
              </div>
            </div>
            <div class="col-md-7">
              <div class="usr-review-rating">
                <h6>Rating : 
                  <span class="less-55">52%</span>
                </h6>
              </div>
              <div class="usr-review-date">
                <h6>4/1/2014</h6>
              </div> 

              <br>
              <br>

              <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            </div>
          </div> <!-- /row /profile page user review container -->
        </div> <!-- /Tab Review Perusahaan -->

        <!-- Tab Interviews -->
        <div class="tab-pane fade" id="interviews">
          <!-- profile page user interview container -->
          <div class="row prof-usr-interview-container">
            <div class="col-md-5">
              <div class="col-xs-6">
                <img class="img-thumbnail user-prof-pic-thumb" src="img/eeb2.png">
              </div>
              <div class="col-xs-6">
                <h5 class="text-blue">Budi Tulus</h5>
                <h6>Surabaya Barat,</h6>
                <h6>Surabaya</h6>
              </div>
            </div>
            <div class="col-md-7">
              <div class="usr-interview-result">
                <h6>Hasil : 
                  <span class="result-accepted">Diterima</span>
                </h6>
              </div>
              <div class="usr-review-date">
                <h6>4/1/2014</h6>
              </div>

              <br>
              <br>

              <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            </div>
          </div> <!-- /row /profile page user interview container -->
          <!-- profile page user interview container -->
          <div class="row prof-usr-interview-container">
            <div class="col-md-5">
              <div class="col-xs-6">
                <img class="img-thumbnail user-prof-pic-thumb" src="img/eeb2.png">
              </div>
              <div class="col-xs-6">
                <h5 class="text-blue">Aryo Buntil</h5>
                <h6>Bandung,</h6>
                <h6>Bandung</h6>
              </div>
            </div>
            <div class="col-md-7">
              <div class="usr-interview-result">
                <h6>Hasil : 
                  <span class="result-rejected">Ditolak</span>
                </h6>
              </div>
              <div class="usr-review-date">
                <h6>4/1/2014</h6>
              </div>

              <br>
              <br>

              <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            </div>
          </div> <!-- /row /profile page user interview container -->
        </div> <!-- /Tab Interviews -->

        <!-- Tab Gaji -->
        <div class="tab-pane fade" id="salaries">
          <!-- Table Rincian Gaji -->
          <table class="table table-condensed">
            <thead>
              <tr>
                <th>#</th>
                <th>Posisi</th>
                <th>Gaji Rata2</th>
                <th>Kisaran Gaji</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Office Boy</td>
                <td>1.5 jt/bln</td>
                <td>1 - 2 jt/bln</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Manager Lvl I</td>
                <td>12 jt/bln</td>
                <td>10 - 13 jt/bln</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Karyawan Lvl I</td>
                <td>6 jt/bln</td>
                <td>3 - 7 jt/bln</td>
              </tr>
              <tr>
                <td>4</td>
                <td>Karyawan Lvl II</td>
                <td>8 jt/bln</td>
                <td>6 - 10 jt/bln</td>
              </tr>
              <tr>
                <td>5</td>
                <td>Senior Manajer</td>
                <td>25 jt/bln</td>
                <td>23 - 30 jt/bln</td>
              </tr>
            </tbody>
          </table> <!-- /Table Rincian Gaji -->
        </div> <!-- /Tab Gaji -->

        <!-- Tab Lowongan Kerja -->
        <div class="tab-pane fade" id="jobs">

        </div> <!-- /Tab Lowongan Kerja -->
      </div>

      <br>

      <div class="row">
        <p class="section-heading">Tertarik Memberi Masukan Untuk Komunitas Kita ?</p>
        <p> Silahkan masukan review anda di <a href="form_temp.php">sini</a>.</p>
      </div>

      <br>
      <br>

    </div> <!-- /left main container -->

    <!-- right main container -->
    <div class="right-main-container">
      <!-- 1st row -->
      <p class="section-heading">Aktivitas Terbaru</p>
      <div class="row">
        <div class="right-main-container-sm">
          <p class="section-title">
            Heading
          </p>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p class="section-title">
            Heading
          </p>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        </div> <!-- /right-main-container-sm -->
      </div> <!-- /1st row -->

      <!-- 2nd row -->
      <p class="section-heading">Inspirasi</p>
      <div class="row">
        <div class="right-main-container-sm">
          <h4>Heading</h4>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <h4>Heading</h4>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        </div> <!-- /right-main-container-sm -->
      </div> <!-- /2nd row -->

    </div> <!-- /right main container -->
  </div> <!-- /prof-main-container -->

  <br>
  <br>

<?php
page_footer();
?>
