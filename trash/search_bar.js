(function() {

	"use strict";

	window.onload = function() {
		$("jobcompany").onkeydown = $("jobcompany").onkeypress = autoComplete;
	};

	function autoComplete() {
		var name = $("jobcompany").value;
		if(name.length > 2) {
			ajaxRequest(name);
		}
	}

	function ajaxRequest(name) {
		var request = new XMLHttpRequest();
		request.onload = displayAutoComplete;
		request.open("GET", "search_bar.php?name=" + name, true);
		request.send();
	}

	function displayAutoComplete() {
		if(this.status == 200) {
			if($("autocomplete")) {
				$("autocomplete").parentNode.removeChild($("autocomplete"));
			}
			var result = JSON.parse(this.responseText);
			var datalist = document.createElement("datalist");
			datalist.id = "autocomplete";
			for(var i = 0; i < result.length; i++) {
				var option = document.createElement("option");
				option.value = option.innerHTML = result[i];
				datalist.appendChild(option);
			}
			$("jobcompanysearchbar").appendChild(datalist);
			/*
				input::-webkit-calendar-picker-indicator {
  					display: none;
				} 
			*/
		}
	}

	function $(id) {
		return document.getElementById(id);
	}

}());