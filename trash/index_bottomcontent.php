
  <!-- Main Container -->
  <div class="container main-container">
    <!-- left main container -->
    <div class="left-main-container">

      <br>

    <!-- 1st row -->
      <p class="section-heading">Perusahaan Sekitar Kamu</p>
      <div class="row">
        <div class="col-md-4">
          <img src="img/news1.jpg" width="100%" length="100%">
          <p class="section-title">
            Jokowi Resmi Menjadi President RI
          </p>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        </div>
        <div class="col-md-4">
          <img src="img/news2.jpg" width="100%" length="100%">
          <p class="section-title">
            Harga Emas Merosot Kembali
          </p>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        </div>
        <div class="col-md-4">
          <img src="img/news3.jpg" width="100%" length="100%">
          <p class="section-title">
            Meraih Omzet Milyaran Rupiah Dari Bisnis Kotoran Ternak
          </p>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
       </div> 
      </div> <!-- /1st row -->

      <br>
      <br>

    </div> <!-- /left main container -->

    <!-- right main container -->
    <div class="right-main-container">
      <!-- 1st row -->
      <p class="section-heading">Aktivitas Terbaru</p>
      <div class="row">
        <div class="right-main-container-sm">
          <p class="section-title">
            Heading
          </p>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p class="section-title">
            Heading
          </p>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        </div> <!-- /right-main-container-sm -->
      </div> <!-- /1st row -->

    </div> <!-- /right main container -->
  </div> <!-- / Main container -->

  <br>
  <br>