<?php

require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits//lib/Classes/DB_Manager/db_manager.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/lib/Classes/Paginator/paginator.php';

/****
**
** Class to manage companies database
**
*****/
class company_manager extends db_manager{
	public $company_id;
	public $limit;
  	public $page;
  	public $links;
  	public $total;
  	public $Paginator;

	// constructor
	public function company_manager($company_id, $limit, $page, $links){
		parent::db_manager();
		$this->company_id = $company_id;

		$this->limit      = $limit;
  		$this->page       = $page;
  		$this->links      = $links;

  		$this->total = 0;

	}

	// get company's data
	public function getData(){

		$query ="SELECT 
			a.company_name, 
			a.company_description, 
			a.company_address_1, 
			a.company_city,
			a.company_zipcode,
			a.company_coordinate,
			a.company_type,
			a.company_year_est,
			a.company_ceo,
			a.company_phone,
			a.company_fax,
			a.company_email,
			a.company_website,
			b.state_name,
			c.country_name,
			d.size_value,
			e.struct_value,
			f.category_sub_category " .
			"FROM company_profile_table as a " .
			"LEFT JOIN indo_states_table as b on a.company_state_id = b.state_id " .
			"LEFT JOIN countries_table as c on a.company_country_id = c.country_id " .
			"LEFT JOIN company_sizes_table as d on a.company_size_id = d.size_id " .
			"LEFT JOIN company_structs_table as e on a.company_struct_id = e.struct_id " .
			"LEFT JOIN company_categories_table as f on a.company_category_id = f.category_id " .
			"WHERE a.company_id = " . $this->company_id . " " .
			"LIMIT 1;";

		$row = parent::queryDB($query);

		$result = $row->fetch(PDO::FETCH_ASSOC);   
		 
		return $result;  

	}

	// get company's reviews
	public function getReviews(){

		$query = "SELECT 
				review_id,
				review_user_id, 
				review_overall_rating, 
				review_job_title, 
				review_location, 
				review_employment_status, 
				review_employment_type, 
				review_employment_length, 
				review_review_title, 
				review_pros,
				review_cons,
				review_advice_to_management, 
				review_rating_career_opportunities,
				review_rating_compensations_benefits,
				review_rating_work_life_balance,
				review_rating_senior_management,
				review_rating_culture_values,
				review_recommend,
				review_ceo_approval,
				review_company_outlook,
				submission_date,
				review_verified,
				review_like " .
			"FROM reviews_table " .
			"WHERE review_company_id = " . $this->company_id . " ";

		$conn = new mysqli( 'localhost', 'root', 'root', 'offfits_data_db' );

		$this->Paginator  = new paginator( $conn, $query );
  		$results    = $this->Paginator->getData( $this->limit, $this->page );

  		return $results;

	}


	// get company's reviews
	public function getInterviews(){

		$query = "SELECT
				interview_id, 
				interview_company_id,
				interview_user_id,
				interview_job_title,
				interview_result,
				interview_location,
				interview_length,
				interview_time_month,
				interview_time_year,
				interview_overall_experience, 
				interview_difficulty, 
				interview_application, 
				interview_process, 
				interview_question, 
				submission_date, 
				interview_verified, 
				interview_like " .
			"FROM interviews_table " .
			"WHERE interview_company_id = " . $this->company_id . " ";

		$conn = new mysqli( 'localhost', 'root', 'root', 'offfits_data_db' );

		$this->Paginator  = new paginator( $conn, $query );
  		$results    = $this->Paginator->getData( $this->limit, $this->page );

  		return $results;
  	}

	// create paginator links
	public function createLinks($list_class, $extra_param=""){
		if ($this->Paginator!= NULL){
			return $this->Paginator->createLinks( $this->links, 'pagination pagination-sm', $extra_param );
		} else {
			return "";
		}
	}

}


?>