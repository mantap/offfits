<?php

require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits//lib/Classes/DB_Manager/db_manager.php';

/****
**
** Class to create tables in the database
**
*****/
class login_manager extends db_manager{

	// constructor
	public function login_manager(){
		parent::db_manager();

		$this->dbname = "offfits_secure_login_db"; // change database
		$this->hostname = "localhost";
		$this->username = "root";
		$this->password = "root";


		// $sql = "CREATE USER 'uadmin'@'localhost' IDENTIFIED BY 'padmin'; " .
		// 	"GRANT SELECT, INSERT, UPDATE , DELETE ON `offfits_secure_login_db`.* TO 'sec_user'@'localhost';";

		// parent::executeDB($sql);

	}


	// create members table 
	public function create_members_table(){
		$sql = "CREATE TABLE members_tb( ".
		"id INT NOT NULL AUTO_INCREMENT, ".
		"username VARCHAR(30) NOT NULL, ".
		"email VARCHAR(50) NOT NULL,".
		"password CHAR(128) NOT NULL, ".
		"salt CHAR(128) NOT NULL," .
		"PRIMARY KEY ( id )); ";

		parent::executeDB($sql);
	}


	// delete members table
	public function delete_members_table(){
		$sql = "DROP TABLE members_tb;";

		parent::executeDB($sql);

	}


	// create login attempts table 
	public function create_login_attempts_table(){
		$sql = "CREATE TABLE login_attempts_tb ( " .
    		"user_id INT(11) NOT NULL, ".
    		"time VARCHAR(30) NOT NULL )";

		parent::executeDB($sql);
	}


	// delete login attempts table
	public function delete_login_attempts_table(){
		$sql = "DROP TABLE login_attempts_tb;";

		parent::executeDB($sql);
	}

	public function insert_test_usr(){
		$sql = "INSERT INTO members_tb( ".
			"username, email, password, salt) ".
			"VALUES ('test_user', ".
			"'test@example.com', ".
			"'00807432eae173f652f2064bdca1b61b290b52d40e429a7d295d76a71084aa96c0233b82f1feac45529e0726559645acaed6f3ae58a286b9f075916ebf66cacc', ".
			"'f9aab579fc1b41ed0c44fe4ecdbfcdb4cb99b9023abb241a6db833288f4eea3c02f76e0d35204a8695077dcf81932aa59006423976224be0390395bae152d4ef')";

		parent::executeDB($sql);
	}

	public function delete_all_tables(){
		$this->delete_members_table();
		$this->delete_login_attempts_table();
	}


}


?>