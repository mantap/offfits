<?php

/****
**
** Class to manage SQL database
**
*****/
class db_manager{
	public $hostname;
	public $dbname;
	public $username;
	public $password;

	// constructor
	public function db_manager(){
		// echo "Creating db_manager object...<br>";

		$this->hostname = "localhost";
		$this->dbname = "offfits_data_db";
		$this->username = "root";
		$this->password = "root";

		// echo "Creating db_manager object finished<br>";
	}

	// function to connect to database
	public function connectDB(){
		// echo "Connecting to DB...<br>";

		$sql = sprintf("mysql:host=%s;dbname=%s", $this->hostname, $this->dbname);

		// echo "Connecting to DB finished<br>";

  		// Connecting to database
  		$db = new PDO ($sql, $this->username, $this->password);
  		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  		return $db;
	}

	// function to disconnect from database
	public function disconnectDB($db){
		// echo "Disconnecting from DB...<br>";

		// disconnect from database
		if($db != NULL){
		  $db = NULL;
		}

		// echo "Disconnecting finished...<br>";
	}

	// function to query from database
	public function executeDB($sql){
		// echo "Start executing command...<br>";

		try{
			$db = $this->connectDB(); // connect to database

			// check if connected to DB
			if($db == NULL){
				echo "ERR:: DB is not connected";
			}

			$db->exec($sql); // start executing
		}
		catch(PDOException $e)
	    {
	    	echo $sql . "<br>" . $e->getMessage() . "<br>";
	    }

	    $this->disconnectDB($db); // disconnect database

		// echo "Executing finished...<br>";
	}


	// function to query from database
	public function queryDB($sql){
		// echo "Start querying...<br>";

		$result = NULL;
		$db = NULL;

		try{
			$db = $this->connectDB(); // connect to database

			// check if connected to DB
			if($db == NULL){
				echo "ERR:: DB is not connected";
				return NULL;
			}

			$result = $db->query($sql); // start query
		}
		catch(PDOException $e)
	    {
	    	echo $sql . "<br>" . $e->getMessage() . "<br>";
	    }

		$this->disconnectDB($db); // disconnect database

		// echo "Querying finished...<br>";

		return $result;
	}

}



?>