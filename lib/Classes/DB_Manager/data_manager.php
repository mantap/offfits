<?php

require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits//lib/Classes/DB_Manager/db_manager.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/lib/Classes/PHPExcel.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/lib/Classes/PHPExcel/IOFactory.php';

/****
**
** Class to insert data into tables in the database
**
*****/
class data_manager extends db_manager{

	// constructor
	public function insert_data(){
		echo "Creating table creator object...<br>";
		parent::db_manager();
		echo "Creating table creator object finished<br>";
	}


	// insert categories data
	public function insert_categories_data(){
		echo "inserting categories data...<br>";

		// echo excel file
		$objPHPExcel = PHPExcel_IOFactory::load($_SERVER["DOCUMENT_ROOT"] . '/OffFits/data/CategoryDescription.xls');
		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
		    $worksheetTitle     = $worksheet->getTitle();
		    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
		    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
		    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		    $nrColumns = ord($highestColumn) - 64;

		    for ($row = 3; $row <= $highestRow; ++ $row) {
		        $col1 = $worksheet->getCellByColumnAndRow(0, $row);
		        $category_id = $col1->getValue();
		        $col2 = $worksheet->getCellByColumnAndRow(1, $row);
		        $category_description = $col2->getValue();
		        $col3 = $worksheet->getCellByColumnAndRow(2, $row);
		        $category_sub_category = $col3->getValue();

		        $sql = "INSERT INTO company_categories_table (category_id, category_description, category_sub_category) " .
		        	" VALUES (" . (int) $category_id . ", '" . $category_description . "', '" . $category_sub_category . "')";

		        parent::executeDB($sql);

		    }

		}

		$objPHPExcel->disconnectWorksheets(); 
   		unset($objPHPExcel);

		echo "Inserting categories data is done<br>";

	}

	// insert indo states data
	public function insert_indo_states_data(){
		echo "inserting indo states data...<br>";

		// echo excel file
		$objPHPExcel = PHPExcel_IOFactory::load($_SERVER["DOCUMENT_ROOT"] . '/OffFits/data/indo_states.xls');
		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
		    $worksheetTitle     = $worksheet->getTitle();
		    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
		    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
		    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		    $nrColumns = ord($highestColumn) - 64;

		    for ($row = 2; $row <= $highestRow; ++ $row) {
		    	$col1 = $worksheet->getCellByColumnAndRow(0, $row);
		        $state_id = $col1->getValue();
		        $col2 = $worksheet->getCellByColumnAndRow(1, $row);
		        $state_name = $col2->getValue();

		        $sql = "INSERT INTO indo_states_table (state_id, state_name)" .
		        	" VALUES (" . (int) $state_id . ", '" . $state_name . "') "; 

		        parent::executeDB($sql);

		    }

		}

		$objPHPExcel->disconnectWorksheets(); 
   		unset($objPHPExcel);

		echo "Inserting indo states data is done<br>";

	}

	// insert indo cities data
	public function insert_indo_cities_data(){
		echo "inserting indo cities data...<br>";

		// echo excel file
		$objPHPExcel = PHPExcel_IOFactory::load($_SERVER["DOCUMENT_ROOT"] . '/OffFits/data/indo_cities.xls');
		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
		    $worksheetTitle     = $worksheet->getTitle();
		    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
		    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
		    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		    $nrColumns = ord($highestColumn) - 64;

		    for ($row = 2; $row <= $highestRow; ++ $row) {
		    	$col1 = $worksheet->getCellByColumnAndRow(0, $row);
		        $city_id = $col1->getValue();
		        $col2 = $worksheet->getCellByColumnAndRow(1, $row);
		        $city_name = $col2->getValue();

		        $sql = "INSERT INTO indo_cities_table (city_id, city_name)" .
		        	" VALUES (" . (int) $city_id . ", '" . $city_name . "') "; 

		        parent::executeDB($sql);

		    }

		}

		$objPHPExcel->disconnectWorksheets(); 
   		unset($objPHPExcel);

		echo "Inserting indo cities data is done<br>";

	}

	// insert countries data
	public function insert_countries_data(){
		echo "inserting countries data...<br>";

		// echo excel file
		$objPHPExcel = PHPExcel_IOFactory::load($_SERVER["DOCUMENT_ROOT"] . '/OffFits/data/countries.xls');
		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
		    $worksheetTitle     = $worksheet->getTitle();
		    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
		    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
		    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		    $nrColumns = ord($highestColumn) - 64;

		    for ($row = 2; $row <= $highestRow; ++ $row) {
		    	$col1 = $worksheet->getCellByColumnAndRow(0, $row);
		        $country_id = $col1->getValue();
		        $col2 = $worksheet->getCellByColumnAndRow(1, $row);
		        $country_name = $col2->getValue();

		        $sql = "INSERT INTO countries_table (country_id, country_name)" .
		        	" VALUES (" . (int) $country_id . ", '" . $country_name . "') "; 

		        parent::executeDB($sql);

		    }

		}

		$objPHPExcel->disconnectWorksheets(); 
   		unset($objPHPExcel);

		echo "Inserting countries data is done<br>";

	}


	// insert company structures data
	public function insert_company_structs_data(){
		echo "inserting company structs data...<br>";

		// echo excel file
		$objPHPExcel = PHPExcel_IOFactory::load($_SERVER["DOCUMENT_ROOT"] . '/OffFits/data/company_structs.xlsx');
		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
		    $worksheetTitle     = $worksheet->getTitle();
		    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
		    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
		    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		    $nrColumns = ord($highestColumn) - 64;

		    for ($row = 2; $row <= $highestRow; ++ $row) {
		    	$col1 = $worksheet->getCellByColumnAndRow(0, $row);
		        $struct_id = $col1->getValue();
		        $col2 = $worksheet->getCellByColumnAndRow(1, $row);
		        $struct_value = $col2->getValue();

		        $sql = "INSERT INTO company_structs_table (struct_id, struct_value)" .
		        	" VALUES (" . (int) $struct_id . ", '" . $struct_value . "') "; 

		        parent::executeDB($sql);

		    }

		}

		$objPHPExcel->disconnectWorksheets(); 
   		unset($objPHPExcel);

		echo "Inserting company structs data is done<br>";

	}


	// insert company sizes data
	public function insert_company_sizes_data(){
		echo "inserting company sizes data...<br>";

		// echo excel file
		$objPHPExcel = PHPExcel_IOFactory::load($_SERVER["DOCUMENT_ROOT"] . '/OffFits/data/company_sizes.xlsx');
		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
		    $worksheetTitle     = $worksheet->getTitle();
		    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
		    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
		    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		    $nrColumns = ord($highestColumn) - 64;

		    for ($row = 2; $row <= $highestRow; ++ $row) {
		    	$col1 = $worksheet->getCellByColumnAndRow(0, $row);
		        $size_id = $col1->getValue();
		        $col2 = $worksheet->getCellByColumnAndRow(1, $row);
		        $size_value = $col2->getValue();

		        $sql = "INSERT INTO company_sizes_table (size_id, size_value)" .
		        	" VALUES (" . (int) $size_id . ", '" . $size_value . "') "; 

		        parent::executeDB($sql);

		    }

		}

		$objPHPExcel->disconnectWorksheets(); 
   		unset($objPHPExcel);

		echo "Inserting company sizes data is done<br>";

	}


	// insert company profile data
	public function insert_company_profile_data(){
		echo "inserting company profile data...<br>";

		// load 1st company data
		$csv_path = $_SERVER["DOCUMENT_ROOT"] . '/OffFits/data/list_perusahaan_1.csv';
		$sql = "LOAD DATA INFILE '" . $csv_path . "' " .
			 "INTO TABLE company_profile_table " .
			 "FIELDS TERMINATED BY ',' " . 
			 "OPTIONALLY ENCLOSED BY '\"' " .
			 "LINES TERMINATED BY '\r' " .
			 "IGNORE 1 LINES " . 
			 	"(@company_category_id, " .
			 	"@company_name, " . 
			 	"@company_phone, " .
			 	"@company_fax, " .
			 	"@company_address_1, " . 
			 	"@company_city, " .
			 	"@company_email, " .
			 	"@company_address_2, " .
			 	"@company_state_id, " .
			 	"@company_website, " .
			 	"@company_zipcode, " .
			 	"@company_country_id) " .
			 "SET " .
				 "company_name = @company_name, " .
				 "company_description = 'NULL', " .
				 "company_address_1 = @company_address_1, " .
				 "company_address_2 = @company_address_2, " .
				 "company_city = @company_city, " .
				 "company_state_id = @company_state_id, " .
				 "company_country_id = 77, " .
				 "company_zipcode = @company_zipcode, " .
				 "company_size_id = 'NULL', " .
				 "company_type = 'NULL', " .
				 "company_struct_id = 'NULL', " .
				 "company_category_id = @company_category_id, " .
				 "company_year_est = 'NULL', " .
				 "company_ceo = 'NULL', " .
				 "company_phone = @company_phone, " .
				 "company_fax = @company_fax, " .
				 "company_email = @company_email, " .
				 "company_website = @company_website;";

        parent::executeDB($sql);

		echo "Inserting company profile data is done<br>";

	}


	



}

?>