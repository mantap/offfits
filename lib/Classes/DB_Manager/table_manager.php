<?php

require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits//lib/Classes/DB_Manager/db_manager.php';

/****
**
** Class to create tables in the database
**
*****/
class table_manager extends db_manager{

	// constructor
	public function table_manager(){
		echo "Creating table creator object...<br>";
		parent::db_manager();
		echo "Creating table creator object finished<br>";
	}


	// create main company table
	public function create_company_profile_table(){
		$sql = "CREATE TABLE company_profile_table( ".
			"company_id INT NOT NULL AUTO_INCREMENT, ".
			"company_name VARCHAR(100) NOT NULL, ".
			"company_description VARCHAR(800), ".
			"company_address_1 VARCHAR(100) NOT NULL, ".
       		"company_address_2 VARCHAR(100), ".
       		"company_city VARCHAR(100) NOT NULL, ".
       		"company_state_id INT NOT NULL, ".
       		"company_country_id INT NOT NULL, ".
       		"company_zipcode INT NOT NULL, ".
       		"company_coordinate VARCHAR(100), ".
       		"company_size_id INT, ".
       		"company_type VARCHAR(100), ".
       		"company_struct_id INT, ".
       		"company_category_id INT, ".
       		"company_year_est INT, ".
       		"company_ceo VARCHAR(100), ".
       		"company_phone VARCHAR(100), ".
       		"company_fax VARCHAR(100), ".
       		"company_email VARCHAR(100), ".
       		"company_website VARCHAR(100), ".
       		"PRIMARY KEY ( company_id )); ";

		parent::executeDB($sql);
	}


	// create company images table
	public function create_company_images_table(){
		$sql = "CREATE TABLE company_images_table( ".
			"img_id INT NOT NULL AUTO_INCREMENT, ".
			"img_company_id INT NOT NULL, ".
			"img_path VARCHAR(100) NOT NULL, ".
			"img_thumb VARCHAR(100) NOT NULL, ".
			"PRIMARY KEY ( img_id )); ";

		parent::executeDB($sql);
	}



	// create company sizes table
	public function create_company_sizes_table(){
		$sql = "CREATE TABLE company_sizes_table( ".
			"size_id INT NOT NULL AUTO_INCREMENT, ".
			"size_value VARCHAR(100) NOT NULL, ".
			"PRIMARY KEY ( size_id )); ";

		parent::executeDB($sql);
	}



	// create company types table
	public function create_company_types_table(){
		$sql = "CREATE TABLE company_types_table( ".
			"type_id INT NOT NULL AUTO_INCREMENT, ".
			"type_value VARCHAR(100) NOT NULL, ".
			"PRIMARY KEY ( type_id )); ";

		parent::executeDB($sql);
	}



	// create company structures table
	public function create_company_structs_table(){
		$sql = "CREATE TABLE company_structs_table( ".
			"struct_id INT NOT NULL AUTO_INCREMENT, ".
			"struct_value VARCHAR(100) NOT NULL, ".
			"PRIMARY KEY ( struct_id )); ";

		parent::executeDB($sql);
	}



	// create company categories table
	public function create_company_categories_table(){
		$sql = "CREATE TABLE company_categories_table( ".
			"category_id INT NOT NULL AUTO_INCREMENT, ".
			"category_description VARCHAR(200) NOT NULL, ".
			"category_sub_category VARCHAR(200) NOT NULL, ".
			"PRIMARY KEY ( category_id )); ";

		parent::executeDB($sql);
	}



	// create states table at indo
	public function create_indo_states_table(){
		$sql = "CREATE TABLE indo_states_table( ".
			"state_id INT NOT NULL AUTO_INCREMENT, ".
			"state_name VARCHAR(100) NOT NULL, ".
			"PRIMARY KEY ( state_id )); ";

		parent::executeDB($sql);
	}



	// create cities at indo's table
	public function create_indo_cities_table(){
	$sql = "CREATE TABLE indo_cities_table( ".
		"city_id INT NOT NULL AUTO_INCREMENT, ".
		"city_name VARCHAR(100) NOT NULL, ".
		"PRIMARY KEY ( city_id )); ";

		parent::executeDB($sql);
	}


	// create countries table
	public function create_countries_table(){
		$sql = "CREATE TABLE countries_table( ".
		"country_id INT NOT NULL AUTO_INCREMENT, ".
		"country_name VARCHAR(100) NOT NULL, ".
		"PRIMARY KEY ( country_id )); ";

		parent::executeDB($sql);
	}


	// create company's salaries table
	public function create_salaries_table(){
		$sql = "CREATE TABLE salaries_table( ".
		"salary_id INT NOT NULL AUTO_INCREMENT, ".
		"salary_company_id INT NOT NULL, ".
		"salary_user_id INT NOT NULL, ".
		"salary_job_title VARCHAR(100) NOT NULL, ".
		"salary_employement_status VARCHAR(100) NOT NULL, ".
		"salary_experience INT NOT NULL, ".
		"salary_location VARCHAR(100) NOT NULL, ".
		"salary_value DECIMAL(19,4) NOT NULL, ".
		"salary_currency_id INT NOT NULL, ".
		"salary_type VARCHAR(100) NOT NULL, ".
		"salary_bonus VARCHAR(100), ".
		"salary_cash_bonus INT, ".
		"salary_cash_bonus_type INT, ".
		"salary_stock_bonus INT, ".
		"salary_stock_bonus_type INT, ".
		"salary_profit_sharing INT, ".
		"salary_profit_sharing_type INT, ".
		"salary_sales_commission INT, ".
		"salary_sales_commission_type INT, ".
		"salary_tips INT, ".
		"salary_tips_type INT, ".
		"submission_date datetime NOT NULL, ".
		"salary_verified VARCHAR(100) NOT NULL, ".
		"PRIMARY KEY ( salary_id )); ";

		parent::executeDB($sql);
	}


	// create company's reviews table
	public function create_reviews_table(){
		$sql = "CREATE TABLE reviews_table( ".
		"review_id INT NOT NULL AUTO_INCREMENT, ".
		"review_company_id INT NOT NULL, ".
		"review_user_id INT NOT NULL, ".
		"review_overall_rating INT NOT NULL, ".
		"review_job_title VARCHAR(100) NOT NULL, ".
		"review_location VARCHAR(100) NOT NULL, ".
		"review_employment_status VARCHAR(100) NOT NULL, ".
		"review_employment_type VARCHAR(100), ".
		"review_employment_length VARCHAR(100), ".
		"review_review_title VARCHAR(200) NOT NULL, ".
		"review_pros VARCHAR(1000) NOT NULL,".
		"review_cons VARCHAR(1000) NOT NULL,".
		"review_advice_to_management VARCHAR(1000), ".
		"review_rating_career_opportunities VARCHAR(100), ".
		"review_rating_compensations_benefits VARCHAR(100), ".
		"review_rating_work_life_balance VARCHAR(100), ".
		"review_rating_senior_management VARCHAR(100), ".
		"review_rating_culture_values VARCHAR(100), ".
		"review_recommend VARCHAR(100) NOT NULL, ".
		"review_ceo_approval VARCHAR(100) NOT NULL, ".
		"review_company_outlook VARCHAR(100) NOT NULL, ".
		"submission_date datetime NOT NULL, ".
		"review_verified VARCHAR(100) NOT NULL, ".
		"review_like INT, ".
		"PRIMARY KEY ( review_id )); ";

		parent::executeDB($sql);
	}


	// create company's interviews table
	public function create_interviews_table(){
		echo "Create interview table...<br>";

		$sql = "CREATE TABLE interviews_table( ".
		"interview_id INT NOT NULL AUTO_INCREMENT, ".
		"interview_company_id INT NOT NULL, ".
		"interview_user_id INT NOT NULL, ".
		"interview_job_title VARCHAR(100) NOT NULL, ".
		"interview_result VARCHAR(100) NOT NULL, ".
		"interview_location VARCHAR(100) NOT NULL, ".
		"interview_length VARCHAR(100) NOT NULL, ".
		"interview_time_month VARCHAR(100) NOT NULL, ".
		"interview_time_year VARCHAR(100) NOT NULL, ".
		"interview_overall_experience VARCHAR(100) NOT NULL, ".
		"interview_difficulty VARCHAR(100) NOT NULL, ".
		"interview_application VARCHAR(1000) NOT NULL, ".
		"interview_process VARCHAR(1000) NOT NULL, ".
		"interview_question VARCHAR(1000) NOT NULL, ".
		"submission_date datetime NOT NULL, ".
		"interview_verified VARCHAR(100) NOT NULL, ".
		"interview_like VARCHAR(100) NOT NULL, ".
		"PRIMARY KEY ( interview_id )); ";

		parent::executeDB($sql);

		echo "Create interview table end<br>";
	}



	// delete company_types table
	public function delete_company_profile_table(){
		echo "Deleting company_profile table...<br>";

		$sql = "DROP TABLE company_profile_table;";

		parent::queryDB($sql);

		echo "Company_profile table deleted<br>";
	}



	// delete company_images table
	public function delete_company_images_table(){
		echo "Deleting company_images table...<br>";

		$sql = "DROP TABLE company_images_table;";

		parent::executeDB($sql);

		echo "Company_images table deleted<br>";
	}

	// delete company_sizes table
	public function delete_company_sizes_table(){
		echo "Deleting company_sizes table...<br>";

		$sql = "DROP TABLE company_sizes_table;";

		parent::executeDB($sql);

		echo "Company_sizes table deleted<br>";
	}

	// delete company_types table
	public function delete_company_types_table(){
		echo "Deleting company_types table...<br>";

		$sql = "DROP TABLE company_types_table;";

		parent::executeDB($sql);

		echo "Company_types table deleted<br>";
	}



	// delete company_structs table
	public function delete_company_structs_table(){
		echo "Deleting company_structs table...<br>";

		$sql = "DROP TABLE company_structs_table;";

		parent::executeDB($sql);

		echo "Company_structs table deleted<br>";
	}

	// delete company_categories table
	public function delete_company_categories_table(){
		echo "Deleting company_categories table...<br>";

		$sql = "DROP TABLE company_categories_table;";

		parent::executeDB($sql);

		echo "Company_categories table deleted<br>";
	}


	// delete indo_states table
	public function delete_indo_states_table(){
		echo "Deleting indo_states table...<br>";

		$sql = "DROP TABLE indo_states_table;";

		parent::executeDB($sql);

		echo "Indo_states table deleted<br>";
	}




	// delete indo_cities table
	public function delete_indo_cities_table(){
		echo "Deleting indo_cities table...<br>";

		$sql = "DROP TABLE indo_cities_table;";

		parent::executeDB($sql);

		echo "Indo_cities table deleted<br>";
	}

	// delete salaries table
	public function delete_countries_table(){
		echo "Deleting countries table...<br>";

		$sql = "DROP TABLE countries_table;";

		parent::executeDB($sql);

		echo "Countries table deleted<br>";
	}

	// delete salaries table
	public function delete_salaries_table(){
		echo "Deleting salary table...<br>";

		$sql = "DROP TABLE salaries_table;";

		parent::executeDB($sql);

		echo "Salary table deleted<br>";
	}


	// delete reviews table
	public function delete_reviews_table(){
		echo "Deleting reviews table...<br>";

		$sql = "DROP TABLE reviews_table;";

		parent::executeDB($sql);

		echo "Reviews table deleted<br>";
	}

	// delete interviews table
	public function delete_interviews_table(){
		echo "Deleting interview table...<br>";

		$sql = "DROP TABLE interviews_table;";

		parent::executeDB($sql);

		echo "Interview table deleted<br>";
	}

	// delete all tables
	public function delete_all_tables(){
		echo "Deleting all tables...<br>";

		$this->delete_company_profile_table();
		$this->delete_company_images_table();
		$this->delete_company_sizes_table();
		$this->delete_company_types_table();
		$this->delete_company_structs_table();
		$this->delete_company_categories_table();
		$this->delete_indo_states_table();
		$this->delete_indo_cities_table();
		$this->delete_countries_table();
		$this->delete_salaries_table();
		$this->delete_reviews_table();
		$this->delete_interviews_table();

		echo "All tables are deleted<br>";

	}

	// create all tables
	public function create_all_tables(){
		echo "Creating all tables...<br>";

		$this->create_company_profile_table();
		$this->create_company_images_table();
		$this->create_company_sizes_table();
		$this->create_company_types_table();
		$this->create_company_structs_table();
		$this->create_company_categories_table();
		$this->create_indo_states_table();
		$this->create_indo_cities_table();
		$this->create_countries_table();
		$this->create_salaries_table();
		$this->create_reviews_table();
		$this->create_interviews_table();

		echo "All tables are created<br>";

	}



}



?>