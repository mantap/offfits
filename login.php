<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/db_connect.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/functions.php';

  if (login_check($mysqli) == true) {
    $logged = 'in';
  } else {
      $logged = 'out';
  }

  sec_session_start();

  head_tag();
  page_header($mysqli);

?>



  <!-- Main Container -->
  <div class="container prof-main-container">
    <!-- left main container -->
    <div class="left-main-container">

      <br>

      <?php
        if (isset($_GET['error'])) {
            echo '<p class="error">Error Logging In!</p>';
        }
        ?> 
        <form action="includes/process_login.php" method="post" name="login_form">                      
            Email: <input type="text" name="email" />
            Password: <input type="password" 
                             name="password" 
                             id="password"/>
            <input type="button" 
                   value="Login" 
                   onclick="formhash(this.form, this.form.password);" /> 
        </form>
 
      <?php
        if (login_check($mysqli) == true) {
                        echo '<p>Currently logged ' . $logged . ' as ' . htmlentities($_SESSION['username']) . '.</p>';
 
            echo '<p>Do you want to change user? <a href="includes/logout.php">Log out</a>.</p>';
        } else {
                        echo '<p>Currently logged ' . $logged . '.</p>';
                        echo "<p>If you don't have a login, please <a href='register.php'>register</a></p>";
                }
      ?>      


    </div> <!-- /left main container -->

    <!-- right main container -->
    <div class="right-main-container">

      <br>

    </div> <!-- /right main container -->
  </div> <!-- / Main container -->

  <br>
  <br>

<?php
page_footer();
?>