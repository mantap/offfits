<?php

require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/lib/Classes/Paginator/paginator.php';
 
    $conn       = new mysqli( 'localhost', 'root', 'root', 'offfits_data_db' );
 
    $limit      = ( isset( $_GET['limit'] ) ) ? $_GET['limit'] : 25;
    $page       = ( isset( $_GET['page'] ) ) ? $_GET['page'] : 1;
    $links      = ( isset( $_GET['links'] ) ) ? $_GET['links'] : 7;
    $query      = "SELECT company_id, company_name, company_address_1, company_city " .
                "FROM company_profile_table";
 
    $Paginator  = new paginator( $conn, $query );
 
    $results    = $Paginator->getData( $limit, $page );

?>
<!DOCTYPE html>
    <head>
        <title>PHP Pagination</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
                <div class="col-md-10 col-md-offset-1">
                <h1>PHP Pagination</h1>
                <table class="table table-striped table-condensed table-bordered table-rounded">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th width="30%">Name</th>
                            <th width="60%">Address</th>
                            <th width="5%">City</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for( $i = 0; $i < count( $results->data ); $i++ ): ?>
                            <tr>
                                <td><?php echo $results->data[$i]['company_id']; ?></td>
                                <td><?php echo $results->data[$i]['company_name']; ?></td>
                                <td><?php echo $results->data[$i]['company_address_1']; ?></td>
                                <td><?php echo $results->data[$i]['company_city']; ?></td>
                            </tr>
                        <?php endfor; ?>
                    </tbody>
                </table>
                    <?php echo $Paginator->createLinks( $links, 'pagination pagination-sm' ); ?>
                </div>
        </div>
        </body>
</html>


