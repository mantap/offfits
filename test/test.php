<?php
/****
**
** This is a test php 
**
****/

require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/lib/Classes/DB_Manager/login_manager.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/lib/Classes/DB_Manager/table_manager.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/lib/Classes/DB_Manager/data_manager.php';

echo "Test Program Start...<br>";

// ---------------- initialize table creator object
$tb_mgr = new table_manager();

$tb_mgr->delete_all_tables(); // delete all tables
$tb_mgr->create_all_tables(); // create all tables



// ---------------- initialize tb insert object
$tb_insert = new data_manager();

$tb_insert->insert_categories_data();
$tb_insert->insert_indo_states_data();
$tb_insert->insert_indo_cities_data();
$tb_insert->insert_countries_data();
$tb_insert->insert_company_structs_data();
$tb_insert->insert_company_sizes_data();
$tb_insert->insert_company_profile_data();


// ---------------- create secure login
// $login_mgr = new login_manager();
// $login_mgr->delete_all_tables();
// $login_mgr->create_members_table();
// $login_mgr->create_login_attempts_table();
// $login_mgr->insert_test_usr();


echo "Test Program End<br>";

?>