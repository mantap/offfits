<?php
include_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/psl-config.php';
 
// =========================== Helper Functions ===================================

function print_interview( $i, $interviews ){
    ?>

 <!--                interview_company_id,
                interview_user_id,
                interview_job_title,
                interview_result,
                interview_location,
                interview_length,
                interview_time_month,
                interview_time_year,
                interview_overall_experience, 
                interview_difficulty, 
                interview_application, 
                interview_process, 
                interview_question, 
                submission_date, 
                interview_verified, 
                interview_like " . -->

    <!-- profile page user interview container -->
    <div class="row prof-usr-interview-container">
        <div class="col-md-5">
            <div class="col-xs-6">
                <img class="img-thumbnail user-prof-pic-thumb" src="img/eeb2.png">
            </div>
            <div class="col-xs-6">
                <h5 class="text-blue"><?= $interviews->data[$i]['interview_job_title'] ?></h5>
                <h6>Surabaya Barat,</h6>
                <h6>Surabaya</h6>
            </div>
        </div>
        <div class="col-md-7">
            <div class="usr-interview-result">
                <h6>Hasil : 
                <span class="result-accepted"><?= $interviews->data[$i]['interview_result'] ?></span>
                </h6>
            </div>
        <div class="usr-review-date">
            <h6><?= $interviews->data[$i]['interview_time_month'] ?>, <?= $interviews->data[$i]['interview_time_year'] ?></h6>
        </div>

        <br>
        <br>

        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        </div>
    </div> <!-- /row /profile page user interview container -->

    <?php
}


function print_review( $i, $reviews ){
    ?>
    <!-- profile page user review container -->
    <div class="row prof-usr-review-container">
        <h6><?= $reviews->data[$i]['submission_date'] ?></h6>
        <h3><font color="blue"><q><?= $reviews->data[$i]['review_review_title'] ?></q></font></h3>

        <h5 class="text-blue"><?= $reviews->data[$i]['review_employment_status'] ?> - <?= $reviews->data[$i]['review_job_title'] ?> di <?= $reviews->data[$i]['review_location'] ?></h5>
        <h5 class="text-blue"><?= $reviews->data[$i]['review_employment_type'] ?>, <?= $reviews->data[$i]['review_employment_length'] ?></h5>
        <br>
        <h6>
          Overall Rating : <?php print_stars( $reviews->data[$i]['review_overall_rating'] )?>
        </h6>
        <br>
        <h6>
          Rekomendasi ke teman:<span class="above-85"><?= $reviews->data[$i]['review_recommend'] ?></span>
          Prospek perusahaan:<span class="above-85"><?= $reviews->data[$i]['review_company_outlook'] ?></span>
          Kemempimpinan perusahaan<span class="above-85"><?= $reviews->data[$i]['review_ceo_approval'] ?></span>
        </h6>
        <br>
        <h5>Kelebihan</h5>
        <p><?= $reviews->data[$i]['review_pros'] ?></p>
        <br>
        <h5>Kekurangan</h5>
        <p><?= $reviews->data[$i]['review_cons'] ?></p>
        <br>
        <h5>Saran untuk pihak management</h5>
        <p><?= $reviews->data[$i]['review_advice_to_management'] ?></p>
        <br>
        <h6>Rating peluang karir : <?= $reviews->data[$i]['review_rating_career_opportunities'] ?></h6>
        <h6>Rating benefit perusahaan : <?= $reviews->data[$i]['review_rating_compensations_benefits'] ?></h6>
        <h6>Rating waktu bekerja : <?= $reviews->data[$i]['review_rating_work_life_balance'] ?></h6>
        <h6>Rating senior management : <?= $reviews->data[$i]['review_rating_senior_management'] ?></h6>
        <h6>Rating kultur perusahaan : <?= $reviews->data[$i]['review_rating_culture_values'] ?></h6>

    </div> <!-- /row /profile page user review container -->

    <?php
}

function print_stars($rating){

  for( $i = 0; $i < round($rating, 0, PHP_ROUND_HALF_DOWN); $i++ ): ?>
    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
  <?php 
  endfor;
  
  for( $i = 0; $i < 5-round($rating, 0, PHP_ROUND_HALF_DOWN); $i++ ): ?>
    <span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
  <?php endfor;

}




// =========================== Header/Footer Functions ===================================

function head_tag() {
    ?>

    <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="description" content="">
                <meta name="author" content="">
                <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
                    
                <title>OffFits - Finding The Best Job</title>

                <!-- Bootstrap core CSS -->
                <link href="css/bootstrap.min.css" rel="stylesheet">

                <!-- Custom styles for this template -->
                <link href="css/gaya.css" rel="stylesheet">

                <!-- Just for debugging purposes. Don't actually copy this line! -->
                <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

                <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
                <![endif]-->

                <script src="search_bar.js" type="text/javascript"></script>

                <!-- Script for secure login -->
                <script type="text/JavaScript" src="js/sha512.js"></script> 
                <script type="text/JavaScript" src="js/forms.js"></script> 

            </head>
    <?php
}

function page_header($mysqli) {
    ?>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">OffFits</a>
                </div>

                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="list.php">Perusahaan</a></li>

                        <!--
                            <li><a href="#about">About</a></li>
                            <li><a href="#contact">Contact</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li class="dropdown-header">Nav header</li>
                                    <li><a href="#">Separated link</a></li>
                                    <li><a href="#">One more separated link</a></li>
                                </ul>
                            </li>
                        -->
                    
                    </ul>

                    <?php 
                        if(login_check($mysqli) == true) {
                            welcome_back_user();
                        } else {
                            sign_in_form();
                        }
                    ?>

                    

                </div><!--/.navbar-collapse -->
            </div>
        </div>

    <?php
}

function page_footer() {
    ?>

            <footer>
                <!-- footer container-->
                <div class="container footer-container">
                    <!-- 1st row -->
                    <div class="row">
                        <!-- 1st column -->
                        <div class="col-xs-2">
                            <ul class="footer-contents">
                                <li><a href="#">About</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Press</a></li>
                            </ul>
                        </div> <!-- /1st column -->
                        <!-- 2nd column -->
                        <div class="col-xs-2">
                            <ul class="footer-contents">
                                <li><a href="#">Help</a></li>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">Terms & Policy</a></li>
                            </ul>
                        </div> <!-- /2nd column -->
                     </div>  <!-- /1st row -->
                    <!-- 2nd row -->
                     <div class="row">
                        <p class="trademark">&copy; OffFits 2014</p>
                    </div>  <!-- /2nd row -->
                </div> <!-- /footer container -->
            </footer>

            <!-- Bootstrap core JavaScript
            ================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
        </body>
    </html>

    <?php
}

function activity_news_tab() {
    ?>

        <!-- right main container -->
        <div class="right-main-container">
            <!-- 1st row -->
            <p class="section-heading">Aktivitas Terbaru</p>
                <div class="row">
                    <div class="right-main-container-sm">
                        <p class="section-title">
                        Heading
                        </p>
                        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                        <p class="section-title">
                        Heading
                        </p>
                        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                    </div> <!-- /right-main-container-sm -->
                </div> <!-- /1st row -->

            <!-- 2nd row -->
            <p class="section-heading">Inspirasi</p>
                <div class="row">
                    <div class="right-main-container-sm">
                        <h4>Heading</h4>
                        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                         <h4>Heading</h4>
                        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                    </div> <!-- /right-main-container-sm -->
                </div> <!-- /2nd row -->

        </div> <!-- /right main container -->
    </div> <!-- /prof-main-container -->

    <br>
    <br>

    <?php
}

function sign_in_form() {
    ?>
    <ul class="nav navbar-right">
        <li>
            <a href="../OffFits/login.php">LogIn</a>
        </li>
    </ul>

    <?php
}

function welcome_back_user(){
    ?>

    <ul class="nav navbar-right">
        <li>
            Currently logged in as <?php echo htmlentities($_SESSION['username']); ?>
        </li>
        <li>
            <a href="includes/logout.php">Log out</a>
        </li>
    </ul>

    <?php
}


// =========================== Secure LogIn Functions ===================================

function sec_session_start() {
    $session_name = 'sec_session_id';   // Set a custom session name
    $secure = SECURE;
    // This stops JavaScript being able to access the session id.
    $httponly = true;
    // Forces sessions to only use cookies.
    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
        exit();
    }
    // Gets current cookies params.
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"],
        $cookieParams["path"], 
        $cookieParams["domain"], 
        $secure,
        $httponly);
    // Sets the session name to the one set above.
    session_name($session_name);
    session_start();            // Start the PHP session 
    session_regenerate_id(true);    // regenerated the session, delete the old one. 
}


function login($email, $password, $mysqli) {
    // Using prepared statements means that SQL injection is not possible. 
    if ($stmt = $mysqli->prepare("SELECT id, username, password, salt 
        FROM members_tb
       WHERE email = ?
        LIMIT 1")) {
        $stmt->bind_param('s', $email);  // Bind "$email" to parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($user_id, $username, $db_password, $salt);
        $stmt->fetch();
 
        // hash the password with the unique salt.
        $password = hash('sha512', $password . $salt);
        if ($stmt->num_rows == 1) {
            // If the user exists we check if the account is locked
            // from too many login attempts 
 
            if (checkbrute($user_id, $mysqli) == true) {
                // Account is locked 
                // Send an email to user saying their account is locked
                return false;
            } else {
                // Check if the password in the database matches
                // the password the user submitted.
                if ($db_password == $password) {
                    // Password is correct!
                    // Get the user-agent string of the user.
                    $user_browser = $_SERVER['HTTP_USER_AGENT'];
                    // XSS protection as we might print this value
                    $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                    $_SESSION['user_id'] = $user_id;
                    // XSS protection as we might print this value
                    $username = preg_replace("/[^a-zA-Z0-9_\-]+/", 
                                                                "", 
                                                                $username);
                    $_SESSION['username'] = $username;
                    $_SESSION['login_string'] = hash('sha512', 
                              $password . $user_browser);
                    // Login successful.
                    return true;
                } else {
                    // Password is not correct
                    // We record this attempt in the database
                    $now = time();
                    $mysqli->query("INSERT INTO login_attempts_tb(user_id, time)
                                    VALUES ('$user_id', '$now')");
                    return false;
                }
            }
        } else {
            // No user exists.
            return false;
        }
    }
}


function checkbrute($user_id, $mysqli) {
    // Get timestamp of current time 
    $now = time();
 
    // All login attempts are counted from the past 2 hours. 
    $valid_attempts = $now - (2 * 60 * 60);
 
    if ($stmt = $mysqli->prepare("SELECT time 
                             FROM login_attempts_tb 
                             WHERE user_id = ? 
                            AND time > '$valid_attempts'")) {
        $stmt->bind_param('i', $user_id);
 
        // Execute the prepared query. 
        $stmt->execute();
        $stmt->store_result();
 
        // If there have been more than 5 failed logins 
        if ($stmt->num_rows > 5) {
            return true;
        } else {
            return false;
        }
    }
}


function login_check($mysqli) {
    // Check if all session variables are set 
    if (isset($_SESSION['user_id'], 
                        $_SESSION['username'], 
                        $_SESSION['login_string'])) {
 
        $user_id = $_SESSION['user_id'];
        $login_string = $_SESSION['login_string'];
        $username = $_SESSION['username'];
 
        // Get the user-agent string of the user.
        $user_browser = $_SERVER['HTTP_USER_AGENT'];
 
        if ($stmt = $mysqli->prepare("SELECT password 
                                      FROM members_tb 
                                      WHERE id = ? LIMIT 1")) {
            // Bind "$user_id" to parameter. 
            $stmt->bind_param('i', $user_id);
            $stmt->execute();   // Execute the prepared query.
            $stmt->store_result();
 
            if ($stmt->num_rows == 1) {
                // If the user exists get variables from result.
                $stmt->bind_result($password);
                $stmt->fetch();
                $login_check = hash('sha512', $password . $user_browser);
 
                if ($login_check == $login_string) {
                    // Logged In!!!! 
                    return true;
                } else {
                    // Not logged in 
                    return false;
                }
            } else {
                // Not logged in 
                return false;
            }
        } else {
            // Not logged in 
            return false;
        }
    } else {
        // Not logged in 
        return false;
    }
}


function esc_url($url) {
 
    if ('' == $url) {
        return $url;
    }
 
    $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);
 
    $strip = array('%0d', '%0a', '%0D', '%0A');
    $url = (string) $url;
 
    $count = 1;
    while ($count) {
        $url = str_replace($strip, '', $url, $count);
    }
 
    $url = str_replace(';//', '://', $url);
 
    $url = htmlentities($url);
 
    $url = str_replace('&amp;', '&#038;', $url);
    $url = str_replace("'", '&#039;', $url);
 
    if ($url[0] !== '/') {
        // We're only interested in relative links from $_SERVER['PHP_SELF']
        return '';
    } else {
        return $url;
    }
}