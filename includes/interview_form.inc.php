<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits//lib/Classes/DB_Manager/db_manager.php';

if ( isset(
	$_POST['interview_company_id'], $_POST['interview_user_id'], 
	$_POST['interview_overall_experience'], $_POST['interview_job_title'],
	$_POST['interview_location'], $_POST['interview_length'],
	$_POST['interview_time_month'], $_POST['interview_time_year'],
	$_POST['interview_overall_experience'], $_POST['interview_difficulty'],
	$_POST['interview_application'], $_POST['interview_process'],
	$_POST['interview_question'], $_POST['interview_result']
	) 
	) {

    // Sanitize and validate the data passed in
    $interview_company_id = $_POST['interview_company_id'];
	$interview_user_id = $_POST['interview_user_id'];
    $interview_overall_experience = filter_input(INPUT_POST, 'interview_overall_experience', FILTER_SANITIZE_STRING);
    $interview_job_title = filter_input(INPUT_POST, 'interview_job_title', FILTER_SANITIZE_STRING);
    $interview_location = filter_input(INPUT_POST, 'interview_location', FILTER_SANITIZE_STRING);
    $interview_length = filter_input(INPUT_POST, 'interview_length', FILTER_SANITIZE_STRING);
    $interview_time_month = filter_input(INPUT_POST, 'interview_time_month', FILTER_SANITIZE_STRING);
    $interview_time_year = filter_input(INPUT_POST, 'interview_time_year', FILTER_SANITIZE_STRING);
    $interview_overall_experience = filter_input(INPUT_POST, 'interview_overall_experience', FILTER_SANITIZE_STRING);
    $interview_difficulty = filter_input(INPUT_POST, 'interview_difficulty', FILTER_SANITIZE_STRING);
    $interview_application = filter_input(INPUT_POST, 'interview_application', FILTER_SANITIZE_STRING);
    $interview_process = filter_input(INPUT_POST, 'interview_process', FILTER_SANITIZE_STRING); 
	$interview_question = filter_input(INPUT_POST, 'interview_question', FILTER_SANITIZE_STRING); 
	$interview_result = filter_input(INPUT_POST, 'interview_result', FILTER_SANITIZE_STRING); 
	$submission_date = date("Y-m-d H:i:s");
	$interview_verified = 'unverified'; 
	$interview_like = 0;


	$db_mgr = new db_manager();

	$query = "INSERT INTO interviews_table (
			interview_company_id, 
			interview_user_id, 
			interview_job_title, 
			interview_result, 
			interview_location, 
			interview_length, 
			interview_time_month, 
			interview_time_year, 
			interview_overall_experience, 
			interview_difficulty, 
			interview_application, 
			interview_process, 
			interview_question, 
			submission_date, 
			interview_verified, 
			interview_like
			) " .
		"VALUES ( " .
			$interview_company_id . ", " .
			$interview_user_id. ", " .
			"'" . $interview_job_title . "', " .
			"'" . $interview_result . "', " .
			"'" . $interview_location . "', " .
			"'" . $interview_length . "', " .
			"'" . $interview_time_month . "', " .
			"'" . $interview_time_year . "', " .
			"'" . $interview_overall_experience . "', " .
			"'" . $interview_difficulty . "', " .
			"'" . $interview_application . "', " .
			"'" . $interview_process . "', " .
			"'" . $interview_question . "', " .
			"'" . $submission_date . "', " .
			"'" . $interview_verified . "', " .
			$interview_like . " " .
			");";

	$db_mgr->queryDB($query);
}

?>




