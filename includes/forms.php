<?php


// print form for company review
function printReviewForm( $review_company_id ){
	?>
	<!-- Form Review Perusahaan -->
      <form role="form" method="post" action="/OffFits/includes/review_form.inc.php">

      	<!-- put hidden form here -->
      	<div class="form-group" style="display:none;">
          <input name="review_company_id" value="<?php echo $review_company_id; ?>" >
          <input name="review_user_id" value="<?php echo htmlentities($_SESSION['user_id']); ?>" >
        </div>

        <div class="form-group">
          <label for="overall-rev-perusahaan">Overall Rating Perusahaan</label>
          <select name="review_overall_rating" class="form-control">
            <option selected="selected">5</option>
            <option>4</option>
            <option>3</option>
            <option>2</option>
            <option>1</option>
          </select>
          <span class="help-block">5 = Baik Sekali, 1 = Buruk Sekali</span>
        </div>

        <div class="form-group">
          <label for="reviewTitle">Review Title</label>
          <input type="review-title" class="form-control" id="reviewTitle" name="review_review_title" placeholder="Silahkan masukan judul review anda">
        </div>

        <div class="form-group">
          <label for="job-title">Posisi Pekerjaan</label>
          <input type="job-title" class="form-control" id="job-title" name="review_job_title" placeholder="i.e. Karyawan Lvl 1">
        </div>

        <div class="form-group">
          <label>Lokasi Pekerjaan</label>
          <input class="form-control" name="review_location" placeholder="Lokasi di mana anda bekerja">
        </div>

        <div class="form-group">
      		<label>Lama Bekerja</label>
  			<select class="form-control" name="review_employment_length">
      			<option selected="selected">Di bawah 1 tahun</option>
		        <option>1 - 2 tahun</option>
		        <option>2 - 3 tahun</option>
		        <option>3 - 5 tahun</option>
		        <option>5 - 6 tahun</option>
		        <option>6 - 9 tahun</option>
		        <option>Di atas 9 tahun</option>
		    </select>
		</div>

        <div class="form-group">
        <label>Status Pekerjaan</label>
          <div class="radio">
            <label>
              <input type="radio" name="review_employment_status" id="optionsRadios1" value="Current Employee" checked="checked">
              Masih bekerja di perusahaan ini
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="review_employment_status" id="optionsRadios2" value="Former Employee">
              Sudah tidak bekerja di perusahaan ini
            </label>
          </div>
        </div>

        <div class="form-group">
        <label>Tipe Pegawai</label>
          <div class="radio">
            <label>
              <input type="radio" name="review_employment_type" id="optionsRadios3" value="Full Time" checked="checked">
              Pegawai Full Time
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="review_employment_type" id="optionsRadios4" value="Part Time">
              Pegawai Part Time
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="review_employment_type" id="optionsRadios5" value="Contract">
              Pegawai Kontrak
            </label>
          </div>
        </div>

        <div class="form-group">
         <label for="pros">Kelebihan</label>
         <textarea class="form-control" name="review_pros" rows="3"></textarea>
       </div>

       <div class="form-group">
         <label for="cons">Kekurangan</label>
         <textarea class="form-control" name="review_cons" rows="3"></textarea>
       </div>

       <div class="form-group">
         <label for="advice">Masukan untuk Manajemen</label>
         <textarea class="form-control" name="review_advice_to_management" rows="3"></textarea>
       </div>

       <div class="form-group">
         <label>Peluang Kemajuan Karir</label>
         <select class="form-control" name="review_rating_career_opportunities">
          <option selected="selected">Menjanjikan</option>
          <option>Biasa Saja</option>
          <option>Tidak Ada</option>
        </select>
      </div>

      <div class="form-group">
       <label>Kompensasi dan Bonus</label>
       <select class="form-control" name="review_rating_compensations_benefits">
          <option selected="selected">Baik Sekali</option>
          <option>Biasa Saja</option>
          <option>Tidak Ada</option>
      </select>
    </div>

    <div class="form-group">
      <label>Jam Kerja</label>
      <select class="form-control" name="review_rating_work_life_balance">
          <option selected="selected">Sering Lembur</option>
          <option>Lembur beberapa kali</option>
          <option>Tidak pernah lembur</option>
      </select>
    </div>

    <div class="form-group">
      <label>Manajemen</label>
      <select class="form-control" name="review_rating_senior_management">
          <option selected="selected">Baik Sekali</option>
          <option>Biasa Saja</option>
          <option>Buruk</option>
      </select>
    </div>

    <div class="form-group">
      <label>Suasana Kerja</label>
      <select class="form-control" name="review_rating_culture_values">
          <option selected="selected">Baik Sekali</option>
          <option>Biasa Saja</option>
          <option>Buruk</option>
      </select>
    </div>

    <div class="form-group">
      <label>Kepemimpinan perusahaan</label>
      <select class="form-control" name="review_ceo_approval">
          <option selected="selected">Baik Sekali</option>
          <option>Biasa Saja</option>
          <option>Buruk</option>
      </select>
    </div>

     <div class="form-group">
      <label>Rekomendasi ke teman</label>
      <select class="form-control" name="review_recommend">
          <option selected="selected">Ya</option>
          <option>Tidak</option>
      </select>
    </div>

    <div class="form-group">
      <label>Prospek Perusahaan</label>
      <select class="form-control" name="review_company_outlook">
          <option selected="selected">Baik Sekali</option>
          <option>Biasa Saja</option>
          <option>Buruk</option>
      </select>
    </div>

    <button type="submit" class="btn btn-default">Submit</button>

  </form> <!-- /form review perusahaan -->

      <?php
}

// print form for interview review
function printInterviewForm( $interview_company_id ){
	?>
		<!-- Form Review Wawancara Kerja -->
      <form role="form" method="post" action="/OffFits/includes/interview_form.inc.php">

        <!-- put hidden form here -->
        <div class="form-group" style="display:none;">
          <input name="interview_company_id" value="<?php echo $interview_company_id; ?>" >
          <input name="interview_user_id" value="<?php echo htmlentities($_SESSION['user_id']); ?>" >
        </div>

        <div class="form-group">
          <label for="overal-interview-exp">Overall Process Wawancara</label>
          <select class="form-control" name="interview_overall_experience">
            <option>5</option>
            <option>4</option>
            <option>3</option>
            <option>2</option>
            <option>1</option>
          </select>
          <span class="help-block">5 = Baik Sekali, 1 = Buruk Sekali</span>
        </div>

        <div class="form-group">
          <label for="posisiIntKerja">Posisi Pekerjaan yang Dilamar</label>
          <input type="posisi-int-kerja" class="form-control" id="posisi-int-kerja" name="interview_job_title" placeholder="i.e. Karyawan lvl 1">
        </div>

        <div class="form-group">
          <label>Lokasi Interview</label>
          <input class="form-control" name="interview_location" placeholder="Lokasi di mana anda melakukan interview">
        </div>

        <div class="form-group">
          <label>Lama process interview</label>
          <input class="form-control" name="interview_length">
        </div>

        <div class="form-group">
          <label>Waktu Interview</label>
          <input class="form-control" name="interview_time_month" placeholder="mm">
          <input class="form-control" name="interview_time_year" placeholder="yyyy">
        </div>

        <div class="form-group">
           <label>Pengalaman Interview Secara keseluruhan</label>
           <select class="form-control" name="interview_overall_experience">
            <option selected="selected">Menyenangkan</option>
            <option>Biasa Saja</option>
            <option>Tidak menyenangkan</option>
          </select>
        </div>

        <div class="form-group">
           <label>Level kesulitan interview</label>
           <select class="form-control" name="interview_difficulty">
            <option selected="selected">Sulit</option>
            <option>Biasa Saja</option>
            <option>Mudah</option>
          </select>
        </div>

        <div class="form-group">
          <label for="hiringAndInterviewProcess">Cara melamar pekerjaan</label>
          <select class="form-control" name="interview_application">
            <option selected="selected">Online</option>
            <option>Referral</option>
            <option>Career Fair</option>
            <option>Lainnya</option>
          </select>
        </div>

        <div class="form-group">
         <label for="hiringAndInterviewProcess">Process Hiring dan Wawancara</label>
         <textarea class="form-control" name="interview_process" rows="3"></textarea>
       </div>

       <div class="form-group">
         <label for="diffInterviewQuestion">Pertanyaan Wawancara Tersulit</label>
         <textarea class="form-control" name="interview_question" rows="2"></textarea>
       </div>

       <div class="form-group">
        <lable>Hasil Interview:</lable>
        <div class="radio">
          <label>
            <input type="radio" name="interview_result" id="optionsRadios5" value="tidak diterima" checked>
            Tidak diterima
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="interview_result" id="optionsRadios6" value="diterima dan memilih bergabung">
            Diterima dan memilih bergabung
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="interview_result" id="optionsRadios7" value="diterima, tapi menolak untuk bergabung">
            Diterima, tapi menolak bergabung
          </label>
        </div>
      </div>

      <button type="submit" class="btn btn-default">Submit</button>
    </form> <!-- /form review wawancara kerja -->

    <?php
}


// print form for info gaji
function printInfoGajiForm( $review_company_id ){
	?>
	<p> test </p>

    <?php
}


// print upload photo form
function printUploadFotoForm( $review_company_id ){
	?>
	<!-- form upload photos -->
    <form role="form">
      <div class="form-group">
        <label for="exampleInputFile">Pilih foto yang akan di upload</label>
        <input type="file" id="uploadPhoto">
        <p class="help-block">Example block-level help text here.</p>
      </div>

      <button type="submit" class="btn btn-default">Upload</button>
    </form> <!-- /form upload photos -->

    <?php
}


?>