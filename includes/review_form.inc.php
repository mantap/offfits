<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits//lib/Classes/DB_Manager/db_manager.php';

if ( isset(
	$_POST['review_overall_rating'], $_POST['review_review_title'], 
	$_POST['review_job_title'], $_POST['review_employment_status'],
	$_POST['review_pros'], $_POST['review_cons'],
	$_POST['review_advice_to_management'], $_POST['review_rating_career_opportunities'],
	$_POST['review_rating_compensations_benefits'], $_POST['review_rating_work_life_balance'],
	$_POST['review_rating_senior_management'], $_POST['review_rating_culture_values'],
	$_POST['review_recommend'], $_POST['review_ceo_approval'],
	$_POST['review_company_outlook'], $_POST['review_company_id'],
	$_POST['review_user_id'], $_POST['review_employment_length'], $_POST['review_location'] 
	) 
	) {
    // Sanitize and validate the data passed in
    $review_overall_rating = filter_input(INPUT_POST, 'review_overall_rating', FILTER_SANITIZE_STRING);
    $review_review_title = filter_input(INPUT_POST, 'review_review_title', FILTER_SANITIZE_STRING);
    $review_job_title = filter_input(INPUT_POST, 'review_job_title', FILTER_SANITIZE_STRING);
    $review_employment_status = filter_input(INPUT_POST, 'review_employment_status', FILTER_SANITIZE_STRING);
    $review_employment_type = filter_input(INPUT_POST, 'review_employment_type', FILTER_SANITIZE_STRING);
    $review_pros = filter_input(INPUT_POST, 'review_pros', FILTER_SANITIZE_STRING);
    $review_cons = filter_input(INPUT_POST, 'review_cons', FILTER_SANITIZE_STRING);
    $review_advice_to_management = filter_input(INPUT_POST, 'review_advice_to_management', FILTER_SANITIZE_STRING);
    $review_rating_career_opportunities = filter_input(INPUT_POST, 'review_rating_career_opportunities', FILTER_SANITIZE_STRING);
    $review_rating_compensations_benefits = filter_input(INPUT_POST, 'review_rating_compensations_benefits', FILTER_SANITIZE_STRING); 
	$review_rating_work_life_balance = filter_input(INPUT_POST, 'review_rating_work_life_balance', FILTER_SANITIZE_STRING); 
	$review_rating_senior_management = filter_input(INPUT_POST, 'review_rating_senior_management', FILTER_SANITIZE_STRING); 
	$review_rating_culture_values = filter_input(INPUT_POST, 'review_rating_culture_values', FILTER_SANITIZE_STRING); 
	$review_recommend = filter_input(INPUT_POST, 'review_recommend', FILTER_SANITIZE_STRING); 
	$review_ceo_approval = filter_input(INPUT_POST, 'review_ceo_approval', FILTER_SANITIZE_STRING); 
	$review_company_outlook = filter_input(INPUT_POST, 'review_company_outlook', FILTER_SANITIZE_STRING);  
	$review_employment_length = filter_input(INPUT_POST, 'review_employment_length', FILTER_SANITIZE_STRING);  
	$review_location = filter_input(INPUT_POST, 'review_location', FILTER_SANITIZE_STRING);  
	$submission_date = date("Y-m-d H:i:s");
	$review_verified = 'unverified'; 
	$review_like = 0;
	$review_company_id = $_POST['review_company_id'];
	$review_user_id = $_POST['review_user_id'];


    echo $review_overall_rating;
    echo $review_review_title;
    echo $review_job_title;
    echo $review_employment_status;
    echo $review_employment_type;
    echo $review_pros;
    echo $review_cons;
    echo $review_advice_to_management;
    echo $review_rating_career_opportunities;
	echo $review_rating_compensations_benefits;
	echo $review_rating_work_life_balance;
	echo $review_rating_senior_management;
	echo $review_rating_culture_values;
	echo $review_recommend;
	echo $review_ceo_approval;
	echo $review_company_outlook;
	echo $review_employment_length;  
	echo $review_location;  
	echo $submission_date;
	echo $review_verified; 
	echo $review_like;
	echo $review_company_id;
	echo $review_user_id;

	$db_mgr = new db_manager();

	$query = "INSERT INTO reviews_table (
			review_company_id,
			review_user_id,
			review_overall_rating,
			review_job_title,
			review_location,
			review_employment_status,
			review_employment_type,
			review_employment_length,
			review_review_title,
			review_pros,
			review_cons,
			review_advice_to_management,
			review_rating_career_opportunities,
			review_rating_compensations_benefits,
			review_rating_work_life_balance,
			review_rating_senior_management,
			review_rating_culture_values,
			review_recommend,
			review_ceo_approval,
			review_company_outlook,
			submission_date,
			review_verified,
			review_like
			) " .
		"VALUES ( " .
			$review_company_id . ", " .
			$review_user_id . ", " .
			$review_overall_rating . ", " .
			"'" . $review_job_title . "', " .
			"'" . $review_location . "', " .
			"'" . $review_employment_status . "', " .
			"'" . $review_employment_type . "', " .
			"'" . $review_employment_length . "', " .
			"'" . $review_review_title . "', " .
			"'" . $review_pros . "', " .
			"'" . $review_cons . "', " .
			"'" . $review_advice_to_management . "', " .
			"'" . $review_rating_career_opportunities . "', " .
			"'" . $review_rating_compensations_benefits . "', " .
			"'" . $review_rating_work_life_balance . "', " .
			"'" . $review_rating_senior_management . "', " .
			"'" . $review_rating_culture_values . "', " .
			"'" . $review_recommend . "', " .
			"'" . $review_ceo_approval . "', " .
			"'" . $review_company_outlook . "', " .
			"'" . $submission_date . "', " .
			"'" . $review_verified . "', " .
			$review_like . " " .
			");";

	$db_mgr->queryDB($query);
}

?>




