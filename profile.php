<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/db_connect.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/functions.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits//lib/Classes/DB_Manager/company_manager.php';

  sec_session_start();

  $limit      = ( isset( $_GET['limit'] ) ) ? $_GET['limit'] : 1;
  $page       = ( isset( $_GET['page'] ) ) ? $_GET['page'] : 1;
  $links      = ( isset( $_GET['links'] ) ) ? $_GET['links'] : 7;

  $company_id = ( isset( $_GET['company_id'] ) ) ? $_GET['company_id'] : 0;
  
  $cmpy_mgr = new company_manager($company_id, $limit, $page, $links);
  $result = $cmpy_mgr->getData();
  $reviews = $cmpy_mgr->getReviews();
  $interviews = $cmpy_mgr->getInterviews();

  // get overall rating
  $total_rating = 0;
  $company_overall_rating = 0;
  if ($reviews != NULL && count( $reviews->data ) > 0){
    for( $i = 0; $i < count( $reviews->data ); $i++ ):
      $total_rating += $reviews->data[$i]['review_overall_rating'];
    endfor;
    $company_overall_rating = ( $total_rating / ( count( $reviews->data ) ) );
  }

  $extra_param = "";
  $extra_param .= ( isset( $_GET['company_id'] ) ) ? ('&company_id=' . $company_id) : "";

  head_tag();
  page_header($mysqli);

?>



  <!-- Main Container -->
  <div class="container prof-main-container">
    <!-- left main container -->
    <div class="left-main-container">

    <!-- main profile page row -->
    <div class="row">
      <div class="col-md-8">
        <p class="section-heading"><?= $result["company_name"] ?></p>
        <!-- Keterangan Perusahaan -->
        <label class="prof-rating"><b>Overall Rating </b></label>
        <span class="small-text">(20 reviews)</span> 
        <!-- company overall rating stars-->
        <?php print_stars( $company_overall_rating )?>

        <br>

        <?php if( !($result["company_description"] == 'NULL') ) { ?>
        <span class="prof-info-label"><b>Deskripsi</b></span>
        <span class="prof-info-data"><?= $result["company_description"] ?></span>
        <?php } ?>

        <?php if( !($result["company_address_1"] == 'NULL') ) { ?>
        <span class="prof-info-label"><b>Lokasi</b></span>
        <span class="prof-info-data"><?= $result["company_address_1"] ?></span>
        <?php } ?>

        <?php if( !($result["company_city"] == 'NULL') || !($result["state_name"] == 'NULL') ) { ?>
        <span class="prof-info-label"></span>
        <span class="prof-info-data"><?= $result["company_city"]?>, <?= $result["state_name"] ?></span>
        <?php } ?>

        <?php if( !($result["company_zipcode"] == 'NULL')) { ?>
        <span class="prof-info-label"></span>
        <span class="prof-info-data"><?= $result["company_zipcode"] ?></span>
        <?php } ?>

        <?php if( !($result["country_name"] == 'NULL') ) { ?>
        <span class="prof-info-label"></span>
        <span class="prof-info-data"><?= $result["country_name"] ?></span>
        <?php } ?>


        <?php if( !($result["size_value"] == 0) ) { ?>
        <span class="prof-info-label"><b>Jumlah Karyawan</b></span>
        <span class="prof-info-data"><?= $result["size_value"] ?></span>
        <?php } ?>

        <?php if( !($result["company_year_est"] == 0) ) { ?>
        <label class="prof-info-label"><b>Tahun Dibangun</b></label>
        <span class="prof-info-data"><?= $result["company_year_est"] ?></span>
        <?php } ?>

        <?php if( !($result["company_type"] == 'NULL') ) { ?>
        <label class="prof-info-label"><b>Tipe</b></label> 
        <span class="prof-info-data"><?= $result["company_type"] ?></span>
        <?php } ?>

        <?php if( !($result["category_sub_category"] == 'NULL') ) { ?>
        <label class="prof-info-label"><b>Industri</b></label>
        <span class="prof-info-data"><?= $result["category_sub_category"] ?></span>
        <?php } ?>

        <?php if( !($result["company_ceo"] == 'NULL') ) { ?>
        <label class="prof-info-label"><b>President Direktur</b></label>
        <span class="prof-info-data"><?= $result["company_ceo"] ?></span>
        <?php } ?>

        <?php if( !($result["category_sub_category"] == 'NULL') ) { ?>
        <label class="prof-info-label"><b>Telephone</b></label>
        <span class="prof-info-data"><?= $result["company_phone"] ?></span>
        <?php } ?>

        <?php if( $result["company_fax"] ) { ?>
        <label class="prof-info-label"><b>Fax</b></label>
        <span class="prof-info-data"><?= $result["company_fax"] ?></span>
        <?php } ?>

        <?php if( $result["company_email"] ) { ?>
        <label class="prof-info-label"><b>Email</b></label>
        <span class="prof-info-data"><?= $result["company_email"] ?></span>
        <?php } ?>

        <?php if( $result["company_website"] ) { ?>
        <label class="prof-info-label"><b>Website</b></label>
        <span class="prof-info-data"><a href="<?= $result["company_website"] ?>"><?= $result["company_website"] ?></span>
        <?php } ?>

      </div><!-- /Keterangan Perusahaan -->
      <div class="col-md-4">
        <img class="img-thumbnail prof-pic" src="img/eeb.jpg">
        <a herf="#">more photos...</a>
      </div>
    </div>

    <!-- row for map -->
    <div class="row map-container">
      <iframe width="100%" height="200" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAwrCdwLrFyM84znFGmQldsZmSX7gfQl1Y&q= <?= $result["company_name"]?> + Indonesia">
      </iframe>
    </div> 

    <br>

    <!-- Nav tabs -->
    <div class="row">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#reviews" data-toggle="tab">Review Perusahaan</a></li>
        <li><a href="#interviews" data-toggle="tab">Interviews</a></li>
        <li><a href="#salaries" data-toggle="tab">Gaji</a></li>
        <li><a href="#jobs" data-toggle="tab">Lowongan Kerja</a></li>
      </ul>
    </div>

    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Tab Review Perusahaan -->
      <div class="tab-pane active" id="reviews">
        <?php 
          if ( $reviews != NULL && count( $reviews->data ) > 0){
            for( $i = 0; $i < count( $reviews->data ); $i++ ):
              print_review( $i, $reviews );
            endfor;

            echo $cmpy_mgr->createLinks( 'pagination pagination-sm', $extra_param ); 
          }else{
            echo '<p>Belum ada review untuk perusahaan ini.</p>';
          }
        ?>
      </div> <!-- /Tab Review Perusahaan -->

        <!-- Tab Interviews -->
        <div class="tab-pane fade" id="interviews">
          <?php
            if ( $interviews != NULL && count( $interviews->data ) > 0){
              for( $i = 0; $i < count( $interviews->data ); $i++ ):
                print_interview( $i, $interviews );
              endfor;

              echo $cmpy_mgr->createLinks( 'pagination pagination-sm', $extra_param ); 
            }else{
              echo '<p>Belum ada review untuk perusahaan ini.</p>';
            }
          ?>
        </div> <!-- /Tab Interviews -->

        <!-- Tab Gaji -->
        <div class="tab-pane fade" id="salaries">
          <!-- Table Rincian Gaji -->
          <table class="table table-condensed">
            <thead>
              <tr>
                <th>#</th>
                <th>Posisi</th>
                <th>Gaji Rata2</th>
                <th>Kisaran Gaji</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Office Boy</td>
                <td>1.5 jt/bln</td>
                <td>1 - 2 jt/bln</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Manager Lvl I</td>
                <td>12 jt/bln</td>
                <td>10 - 13 jt/bln</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Karyawan Lvl I</td>
                <td>6 jt/bln</td>
                <td>3 - 7 jt/bln</td>
              </tr>
              <tr>
                <td>4</td>
                <td>Karyawan Lvl II</td>
                <td>8 jt/bln</td>
                <td>6 - 10 jt/bln</td>
              </tr>
              <tr>
                <td>5</td>
                <td>Senior Manajer</td>
                <td>25 jt/bln</td>
                <td>23 - 30 jt/bln</td>
              </tr>
            </tbody>
          </table> <!-- /Table Rincian Gaji -->
        </div> <!-- /Tab Gaji -->

        <!-- Tab Lowongan Kerja -->
        <div class="tab-pane fade" id="jobs">

        </div> <!-- /Tab Lowongan Kerja -->
      </div>

      <br>

      <div class="row">
        <p class="section-heading">Tertarik Memberi Masukan Untuk Komunitas Kita ?</p>
        <p> Silahkan masukan review anda dengan mengklik tombol di bawah</p>

        <!-- put hidden form here -->
        <form id="profile_hidden_form" role="form" method="post" action="/OffFits/form.php">
        <div class="form-group" style="display:none;">
          <input name="company_id" value="<?php echo $company_id; ?>">
          <input name="company_name" value="<?= $result["company_name"] ?>">
        </div>
        <button type="submit" class="btn btn-default">sini</button>
      </div>

      <br>
      <br>


    </div> <!-- /left main container -->

    <!-- right main container -->
    <div class="right-main-container">

     <!-- 1st row -->
      <p class="section-heading">Aktivitas Terbaru</p>
      <div class="row">
        <div class="right-main-container-sm">
          <p class="section-title">
            Heading
          </p>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p class="section-title">
            Heading
          </p>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        </div> <!-- /right-main-container-sm -->
      </div> <!-- /1st row -->

      <!-- 2nd row -->
      <p class="section-heading">Inspirasi</p>
      <div class="row">
        <div class="right-main-container-sm">
          <h4>Heading</h4>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <h4>Heading</h4>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
        </div> <!-- /right-main-container-sm -->
      </div> <!-- /2nd row -->

    </div> <!-- /right main container -->
  </div> <!-- / Main container -->

  <br>
  <br>

<?php
page_footer();
?>