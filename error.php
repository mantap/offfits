<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/db_connect.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/OffFits/includes/functions.php';

  $error = filter_input(INPUT_GET, 'err', $filter = FILTER_SANITIZE_STRING);
 
  if (! $error) {
    $error = 'Oops! An unknown error happened.';
  }

  sec_session_start();

  head_tag();
  page_header($mysqli);

?>



  <!-- Main Container -->
  <div class="container prof-main-container">
    <!-- left main container -->
    <div class="left-main-container">

      <br>

      <h1>There was a problem</h1>
      <p class="error"><?php echo $error; ?></p>  


    </div> <!-- /left main container -->

    <!-- right main container -->
    <div class="right-main-container">

      <br>

    </div> <!-- /right main container -->
  </div> <!-- / Main container -->

  <br>
  <br>

<?php
page_footer();
?>